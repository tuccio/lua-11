#pragma once

#include <lua.hpp>

#include "Defines.h"

LUAPP11_INLINE void * operator new (size_t size, lua_State *L)
{
	return lua_newuserdata(L, size);
}