#pragma once

#include "Types.h"

#include <functional>
#include <type_traits>

namespace luapp11
{

	namespace detail
	{

		template <int N>
		struct Placeholder
		{

		};

	}

}


namespace std
{

	template <int N>
	struct is_placeholder<luapp11::detail::Placeholder<N>> :
		std::integral_constant<int, N + 1>
	{
		
	};

}

namespace luapp11
{

	namespace detail
	{

		template<typename T, typename R, typename ... Args, int ... N>
		std::function<R(T*, Args ...)> Bind(R (T::*f) (Args ...), IntegerSequence<N ...>)
		{
			return std::bind(f, Placeholder<N>() ...);
		}

		template<typename T, typename R, typename ... Args, int ... N>
		std::function<R(const T*, Args ...)> Bind(R (T::*f) (Args ...) const, IntegerSequence<N ...>)
		{
			return std::bind(f, Placeholder<N>() ...);
		}

	}

}