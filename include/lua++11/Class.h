#pragma once

#include <lua.hpp>

#include "Constructor.h"
#include "Operators.h"
#include "Policy.h"
#include "Registrable.h"
#include "RegistrationForwardDeclarations.h"
#include "RegistrationTree.h"
#include "Types.h"

#include <functional>
#include <vector>

namespace luapp11
{

	namespace detail
	{

		//template <typename C, typename ... BaseClasses>
		int ClassIndexMetafunction(lua_State *L);

		int ClassNewIndexMetafunction(lua_State *L);

		template <typename R, typename C>
		LUAPP11_INLINE const R& GetMemberProperty(const C *object, R C::*pMember)
		{
			return object->*pMember;
		}

		template <typename R, typename C>
		LUAPP11_INLINE void SetMemberProperty(C *object, R C::*pMember, const R & value)
		{
			object->*pMember = value;
		}

	}

	/* Class */

	class IClass :
		public Registrable
	{

	public:

		LUAPP11_INLINE RegistrationFunctionType GetRegistrationFunction(void)
		{
			return m_function;
		}

	protected:

		RegistrationFunctionType m_function;

	};

	template <typename C, typename ... BaseClasses>
	class Class :
		public IClass
	{

	public:

		Class(const char *name)
		{

			m_customIndex    = nullptr;
			m_customNewIndex = nullptr;

			m_type           = CLASS;

			m_function       = std::bind((&detail::RegistrationTree::RegisterCppClass<C, BaseClasses...>),
				std::placeholders::_1,
				name,
				&m_constructors, &m_functions, &m_properties,
				&m_customIndex, &m_customNewIndex);
			
		}

		template <typename ... Args>
		Class<C, BaseClasses ...>& Constructor(void)
		{
			m_constructors.push_back(&PushNewConstructor<C, Args ...>);
			return *this;
		}

		// Enabled only if at least one of the operands is of type C

		template <typename T1, typename T2, Operators op>
		typename std::enable_if<
			std::is_same<typename detail::RemoveQualifiers<T1>::type, C>::value ||
			std::is_same<typename detail::RemoveQualifiers<T2>::type, C>::value,
			Class<C, BaseClasses ...>&>::type
			Operator(BinaryOperation<T1, T2, op> &&)
		{
			return Function(OperatorsWrappersGenerator<op>::metafunction, &(OperatorsWrappersGenerator<op>::OperatorWrapper<T1, T2>));
		}

		template <typename T, Operators op>
		typename std::enable_if<
			std::is_same<typename detail::RemoveQualifiers<T>::type, C>::value,
			Class<C, BaseClasses ...>&>::type
			Operator(UnaryOperation<T, op> &&)
		{
			return Function(OperatorsWrappersGenerator<op>::metafunction, &(OperatorsWrappersGenerator<op>::OperatorWrapper<T>));
		}

		template <typename F>
		Class<C, BaseClasses ...>& Function(const char *name, F f)
		{
			auto wrappedFunction = luapp11::Function(name, f);
			m_functions.push_back(wrappedFunction.GetRegistrationFunction());
			return *this;
		}

		template <typename R>
		typename std::enable_if<!detail::IsFunction<R>::value && std::is_class<R>::value, Class<C, BaseClasses ...>&>::type Property(const char *name, R C::*pMember)
		{

			std::function<const R & (const C * )> cget = std::bind((&ConstReferencePolicy::Get<R, C>), std::placeholders::_1, pMember);
			std::function<R & (C *)> get = std::bind((&ReferencePolicy::Get<R, C>), std::placeholders::_1, pMember);

			std::function<void (C *, const R &)> set = std::bind((&detail::SetMemberProperty<R, C>), std::placeholders::_1, pMember, std::placeholders::_2);

			RegistrationFunctionType registrationFunction = std::bind(
				(&detail::RegistrationTree::RegisterProperty<std::function<const R & (const C *)>, std::function<R & (C *)>, std::function<void(C *, const R &)>>),
				std::placeholders::_1,
				name,
				cget,
				get,
				set);

			m_properties.push_back(registrationFunction);

			return *this;

		}

		template <typename R>
		typename std::enable_if<!detail::IsFunction<R>::value && !std::is_class<R>::value, Class<C, BaseClasses ...>&>::type Property(const char *name, R C::*pMember)
		{

			std::function<R (const C *)> get = std::bind((&ConstReferencePolicy::Get<R, C>), std::placeholders::_1, pMember);
			std::function<void(C *, R)> set = std::bind((&detail::SetMemberProperty<R, C>), std::placeholders::_1, pMember, std::placeholders::_2);

			RegistrationFunctionType registrationFunction = std::bind(
				(&detail::RegistrationTree::RegisterProperty<std::function<R (const C *)>, std::function<void(C *, R)>>),
				std::placeholders::_1,
				name,
				get,
				set);

			m_properties.push_back(registrationFunction);

			return *this;

		}

		template <typename Getter>
		typename std::enable_if<detail::IsFunction<Getter>::value, Class<C, BaseClasses ...>&>::type Property(const char *name, Getter getter)
		{

			RegistrationFunctionType registrationFunction = std::bind(
				(&detail::RegistrationTree::RegisterProperty<Getter>),
				std::placeholders::_1,
				name,
				getter);

			m_properties.push_back(registrationFunction);

			return *this;

		}

		template <typename Getter, typename Setter>
		Class<C, BaseClasses ...>& Property(const char *name, Getter getter, Setter setter)
		{

			RegistrationFunctionType registrationFunction = std::bind(
				(&detail::RegistrationTree::RegisterProperty<Getter, Setter>),
				std::placeholders::_1,
				name,
				getter,
				setter);

			m_properties.push_back(registrationFunction);

			return *this;

		}

		Class<C, BaseClasses ...>& Index(lua_CFunction customIndex)
		{
			m_customIndex = customIndex;
			return *this;
		}

		Class<C, BaseClasses ...>& NewIndex(lua_CFunction customNewIndex)
		{
			m_customNewIndex = customNewIndex;
			return *this;
		}

		template <typename F>
		Class<C, BaseClasses ...>& ToString(F f)
		{
			return Function("__tostring", f);
		}

	private:

		friend class State;
		friend class detail::RegistrationTree;

		std::vector<RegistrationFunctionType> m_constructors;
		std::vector<RegistrationFunctionType> m_functions;
		std::vector<RegistrationFunctionType> m_properties;

		lua_CFunction                         m_customIndex;
		lua_CFunction                         m_customNewIndex;

	};

	/* Constructor */

	class IConstructor :
		public Registrable
	{

	public:

		LUAPP11_INLINE SignatureCheckType GetSignatureCheck(void)
		{
			return m_sigcheck;
		}

	protected:

		SignatureCheckType       m_sigcheck;

	};

	template <typename ... Args>
	class Constructor :
		public IConstructor
	{

	public:

		Constructor(void)
		{
			m_type = Registrable::CONSTRUCTOR;
			m_sigcheck = &SignatureCheck<void, Args ...>;
		}

	};

	LUAPP11_INLINE int ClassDestructor(lua_State *L)
	{
		detail::LuaObjectWrapper *wrapper = static_cast<detail::LuaObjectWrapper*>(lua_touserdata(L, 1));
		wrapper->~LuaObjectWrapper();
		return 0;
	}

}