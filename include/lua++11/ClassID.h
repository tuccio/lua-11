#pragma once

#include <type_traits>

namespace luapp11
{

	namespace detail
	{

		typedef int ClassID;

		template <typename T>
		struct RTTI
		{
			static const ClassID id;
		};

		ClassID GetNextID(void);

		template <typename T>
		const ClassID RTTI<T>::id = GetNextID();

	}

}