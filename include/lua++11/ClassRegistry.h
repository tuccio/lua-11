#pragma once

#include <cassert>
#include <deque>
#include <string>
#include <typeindex>
#include <map>
#include <lua.hpp>

#include "ClassID.h"
#include "Defines.h"
#include "RegistryReference.h"
#include "Types.h"

#include <unordered_map>
#include <unordered_set>

namespace luapp11
{

	namespace detail
	{

		class LuaObjectWrapper;

		typedef LuaObjectWrapper(*UpcastFunction) (LuaObjectWrapper&);
		typedef std::unordered_map<ClassID, UpcastFunction> UpcastMap;

		struct ClassRegistryInfo
		{

			ClassID           cid;
			std::string       name;

			RegistryReference metatable;

			UpcastMap         upcastMap;
			UpcastMap         sharedptrUpcastMap;

		};

		class ClassRegistry
		{

		public:

			ClassRegistry(void);
			~ClassRegistry(void);

			bool Create(lua_State *L);
			void Destroy(void);

			template <typename T>
			ClassRegistryInfo* Register(void);

			LUAPP11_INLINE       ClassRegistryInfo* GetClass(ClassID id);
			LUAPP11_INLINE const ClassRegistryInfo* GetClass(ClassID id) const;

			static ClassRegistry*     GetInstance(lua_State *L);

			//bool IsBaseOf(ClassID base, ClassID derived) const;

			bool GetPathToBase(ClassID base, ClassID derived, std::deque<ClassID> *path = nullptr);

		private:

			std::map<ClassID, ClassRegistryInfo> m_classes;

			lua_State *m_lua;


		};

	}

}

#include "ClassRegistry.inl"