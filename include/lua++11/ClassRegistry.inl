namespace luapp11
{

	namespace detail
	{

		template <typename T>
		ClassRegistryInfo* ClassRegistry::Register(void)
		{

			ClassRegistryInfo *classReg = nullptr;
			ClassID            id       = RTTI<T>::id;

			std::type_index typeIndex(typeid(T));
			auto insertionResult = m_classes.insert(std::make_pair(id, ClassRegistryInfo()));

			if (!insertionResult.second)
			{
				assert(false && "Registering a type twice");
				id = LUAPP11_CLASSID_INVALID;
			}
			else
			{
				classReg = &(insertionResult.first->second);
			}

			return classReg;

		}

		ClassRegistryInfo* ClassRegistry::GetClass(ClassID id)
		{
			auto it = m_classes.find(id);
			return (it != m_classes.end() ? &it->second : nullptr);
		}

		const ClassRegistryInfo* ClassRegistry::GetClass(ClassID id) const
		{
			auto it = m_classes.find(id);
			return (it != m_classes.end() ? &it->second : nullptr);
		}

	}

}