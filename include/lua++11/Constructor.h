#pragma once

#include <lua.hpp>

#include "ClassRegistry.h"
#include "Converters.h"
#include "Debug.h"
#include "Defines.h"
#include "Helpers.h"
#include "Signature.h"
//nclude "StackOperations.h"

#include <type_traits>

namespace luapp11
{

	namespace detail
	{

		int ConstructorChooser(lua_State *L);

		template <typename C, typename Tuple, int ... N>
		void ClassConstructorConcreteCaller(lua_State *L, Tuple args, detail::IntegerSequence<N ...>)
		{

			CppToLuaConverter<C>::Apply(L, std::get<N>(std::forward<Tuple>(args)) ...);

		}

	}
	
	template <typename C, typename ... Args>
	int ClassConstructor(lua_State *L)
	{

		auto sequence = detail::IntegerSequenceGenerator<sizeof ... (Args)>::type();

		auto args = detail::LuaToCppTupleConverter<Args ...>::Apply(L, sequence);
		detail::ClassConstructorConcreteCaller<C>(L, args, sequence);

		return 1;

	}

	template <typename C, typename ... Args>
	void PushNewConstructor(lua_State *L)
	{

		lua_newtable(L);
		
		lua_pushcfunction(L, (&ClassConstructor<C, Args ...>));
		lua_rawseti(L, -2, LUAPP11_CONSTRUCTORTABLE_CONSTRUCTOR);
		
		lua_pushcfunction(L, (&SignatureCheck<void, Args ...>));
		lua_rawseti(L, -2, LUAPP11_CONSTRUCTORTABLE_SIGNATURE);

		lua_pushinteger(L, sizeof ... (Args));
		lua_rawseti(L, -2, LUAPP11_CONSTRUCTORTABLE_ARGS);

	}

}