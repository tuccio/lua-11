#pragma once

#include <lua.hpp>

#include <string>
#include <sstream>
#include <type_traits>

#include "ClassID.h"
#include "ClassRegistry.h"
#include "RegistryReference.h"
#include "Types.h"
#include "Wrapper.h"
#include "LuaBehaviour.h"

namespace luapp11
{

	template <typename T, typename Enable = typename std::enable_if<std::is_class<T>::value>::type>
	struct LuaBehaviourConverter :
		public std::false_type
	{
	};

	namespace detail
	{

		template <typename T>
		LUAPP11_INLINE typename std::enable_if<!std::is_base_of<std::false_type, LuaBehaviourConverter<T>>::value>::type
			SetBehaviour(lua_State *L, int index, const T *object)
		{

			int absIndex = lua_absindex(L, index);

			LuaBehaviour behaviour = LuaBehaviourConverter<T>::GetBehaviour(object);

			if (behaviour)
			{
				Push(behaviour);
				lua_setuservalue(L, absIndex);
			}

		}

		template <typename T>
		LUAPP11_INLINE typename std::enable_if<std::is_base_of<std::false_type, LuaBehaviourConverter<T>>::value>::type
			SetBehaviour(lua_State *L, int index, const T *object) { }

	}

	template <typename T, typename Enable = void>
	struct CppToLuaConverter;

	template <typename T, typename Enable = void>
	struct DefaultCppToLuaConverter;

	/* Null */

	template <>
	struct CppToLuaConverter<std::nullptr_t>
	{

		static void Apply(lua_State *L, std::nullptr_t)
		{
			lua_pushnil(L);
		}

	};

	/* Bool */

	template <>
	struct CppToLuaConverter<bool>
	{

		static void Apply(lua_State *L, bool value)
		{
			lua_pushboolean(L, value);
		}

	};

	template<> struct CppToLuaConverter<const bool> : CppToLuaConverter<bool> { };
	template<> struct CppToLuaConverter<const bool&> : CppToLuaConverter<bool>{};

	/* Floating numbers */

	template <typename T>
	struct CppToLuaConverter<T, typename std::enable_if<std::is_floating_point<T>::value>::type>
	{

		static void Apply(lua_State *L, T value)
		{
			lua_pushnumber(L, static_cast<lua_Number>(value));
		}

	};

	template <typename T>
	struct CppToLuaConverter<const T, typename std::enable_if<std::is_floating_point<T>::value>::type> :
		public CppToLuaConverter<T> { };

	template <typename T>
	struct CppToLuaConverter<const T&, typename std::enable_if<std::is_floating_point<T>::value>::type> :
		public CppToLuaConverter<T> { };

	/* Integer numbers */

	template <typename T>
	struct CppToLuaConverter<T, typename std::enable_if<std::is_integral<T>::value>::type>
	{

		static void Apply(lua_State *L, T value)
		{
			lua_pushinteger(L, static_cast<lua_Integer>(value));
		}

	};

	template <typename T>
	struct CppToLuaConverter<const T, typename std::enable_if<std::is_integral<T>::value>::type> :
		public CppToLuaConverter<T> { };

	template <typename T>
	struct CppToLuaConverter<const T&, typename std::enable_if<std::is_integral<T>::value>::type> :
		public CppToLuaConverter<T> { };

	/* Enums */

	template <typename T>
	struct CppToLuaConverter<T, typename std::enable_if<std::is_enum<T>::value>::type>
	{

		static void Apply(lua_State *L, T value)
		{
			lua_pushinteger(L, static_cast<lua_Integer>(value));
		}

	};

	template <typename T>
	struct CppToLuaConverter<const T, typename std::enable_if<std::is_enum<T>::value>::type> :
		public CppToLuaConverter<T> { };

	template <typename T>
	struct CppToLuaConverter<const T&, typename std::enable_if<std::is_enum<T>::value>::type> :
		public CppToLuaConverter<T> { };

	/* Strings */

	template <>
	struct CppToLuaConverter<const char*>
	{

		static void Apply(lua_State *L, const char * value)
		{
			lua_pushstring(L, value);
		}

	};

	template <>
	struct CppToLuaConverter<std::string>
	{

		static void Apply(lua_State *L, const std::string &value)
		{
			lua_pushstring(L, value.c_str());
		}

	};

	template <>
	struct CppToLuaConverter<std::string &> :
		CppToLuaConverter<std::string> { };

	template <>
	struct CppToLuaConverter<const std::string &> :
		CppToLuaConverter<std::string> { };


	/* Shared pointers */

	template <typename T>
	struct CppToLuaConverter<std::shared_ptr<T>>
	{

		static void Apply(lua_State *L, std::shared_ptr<T> && ptr)
		{

			void *data = lua_newuserdata(L, sizeof(detail::LuaObjectWrapper));

			detail::ClassRegistry     *cr  = detail::ClassRegistry::GetInstance(L);
			detail::ClassRegistryInfo *cri = cr->GetClass(detail::RTTI<T>::id);

			if (!cri)
			{
				std::stringstream ss;
				ss << "Trying to push an unregistered class: " << std::type_index(typeid(T)).name();
				luaL_error(L, ss.str().c_str());
			}
			else
			{

				new (data) detail::LuaObjectWrapper (std::forward<std::shared_ptr<T>>(ptr));

				Push(cri->metatable);
				lua_setmetatable(L, -2);

			}

		}

	};

	/* Registered classes */

	template <typename T>
	struct CppToLuaConverter<T*, typename std::enable_if<std::is_class<T>::value && !detail::IsSharedPtr<T>::value>::type> :
		public DefaultCppToLuaConverter<T*, typename std::enable_if<std::is_class<T>::value && !detail::IsSharedPtr<T>::value>::type>
	{
	};

	template <typename T>
	struct CppToLuaConverter<T&, typename std::enable_if<std::is_class<T>::value && !detail::IsSharedPtr<T>::value>::type> :
		public DefaultCppToLuaConverter < T&, typename std::enable_if<std::is_class<T>::value && !detail::IsSharedPtr<T>::value>::type >
	{
	};

	template <typename T>
	struct CppToLuaConverter<T, typename std::enable_if<std::is_class<T>::value && !detail::IsSharedPtr<T>::value>::type> :
		public DefaultCppToLuaConverter < T, typename std::enable_if<std::is_class<T>::value && !detail::IsSharedPtr<T>::value>::type >
	{
	};

	template <typename T>
	struct DefaultCppToLuaConverter<T*, typename std::enable_if<std::is_class<T>::value>::type>
	{

		static LUAPP11_INLINE void Apply(lua_State *L, T &value)
		{
			Apply(L, &value);
		}

		static void Apply(lua_State *L, T *value)
		{

			if (value)
			{
				void *data = lua_newuserdata(L, sizeof(detail::LuaObjectWrapper));

				detail::ClassRegistry     *cr  = detail::ClassRegistry::GetInstance(L);
				detail::ClassRegistryInfo *cri = cr->GetClass(detail::RTTI<std::remove_const<T>::type>::id);

				if (!cri)
				{

					std::stringstream ss;
					ss << "Trying to push an unregistered class: " << std::type_index(typeid(T)).name();
					luaL_error(L, ss.str().c_str());

				}
				else
				{

					new (data)detail::LuaObjectWrapper(value);

					Push(cri->metatable);
					lua_setmetatable(L, -2);

					detail::SetBehaviour<T>(L, -1, value);

				}

			}
			else
			{
				lua_pushnil(L);
			}

		}

	};

	template <typename T>
	struct DefaultCppToLuaConverter < T&, typename std::enable_if<std::is_class<T>::value>::type > :
		public DefaultCppToLuaConverter<T*> { };

	template <typename T>
	struct DefaultCppToLuaConverter < const T&, typename std::enable_if<std::is_class<T>::value>::type > :
		public DefaultCppToLuaConverter<const T*> { };

	template <typename T>
	struct DefaultCppToLuaConverter <T, typename std::enable_if<std::is_class<T>::value>::type>
	{

		template <typename ... Types>
		static typename std::enable_if<(std::is_constructible<T, Types...>::value && (sizeof ... (Types) > 0))>::type Apply(lua_State *L, Types && ... args)
		{

			void *data = lua_newuserdata(L, sizeof(detail::LuaObjectWrapper));

			detail::ClassRegistry     *cr  = detail::ClassRegistry::GetInstance(L);
			detail::ClassRegistryInfo *cri = cr->GetClass(detail::RTTI<std::remove_const<T>::type>::id);

			if (!cri)
			{

				std::stringstream ss;
				ss << "Trying to push an unregistered class: " << std::type_index(typeid(T)).name();
				luaL_error(L, ss.str().c_str());

			}
			else
			{

				T *object = new T(std::forward<Types>(args) ...);
				new (data) detail::LuaObjectWrapper(&object);

				Push(cri->metatable);
				lua_setmetatable(L, -2);

				detail::SetBehaviour<T>(L, -1, object);

			}

		}

		static void Apply(lua_State *L)
		{

			void *data = lua_newuserdata(L, sizeof(detail::LuaObjectWrapper));

			detail::ClassRegistry     *cr  = detail::ClassRegistry::GetInstance(L);
			detail::ClassRegistryInfo *cri = cr->GetClass(detail::RTTI<std::remove_const<T>::type>::id);

			if (!cri)
			{

				std::stringstream ss;
				ss << "Trying to push an unregistered class: " << std::type_index(typeid(T)).name();
				luaL_error(L, ss.str().c_str());

			}
			else
			{

				T *object = new T;
				new (data) detail::LuaObjectWrapper(&object);

				Push(cri->metatable);
				lua_setmetatable(L, -2);

				detail::SetBehaviour<T>(L, -1, object);

			}

		}

	};

	namespace detail
	{

		template <typename Type = void, typename ... Types>
		struct CppToLuaTupleConverter;

		template <>
		struct CppToLuaTupleConverter<void>

		{
			CppToLuaTupleConverter(lua_State *L) { }

			static void Apply(lua_State *L) { }

		};

		template <typename Type>
		struct CppToLuaTupleConverter < Type > :
			public CppToLuaConverter<Type>
		{

		};

		template <typename Type, typename ... Types>
		struct CppToLuaTupleConverter<Type, Types ...>
		{

			static void Apply(lua_State *L, Type && arg1, Types && ... args)
			{
				CppToLuaConverter<Type>::Apply(L, std::forward<Type>(arg1));
				CppToLuaTupleConverter<Types ...>::Apply(L, std::forward<Types>(args) ...);
			}

		};

	}

}