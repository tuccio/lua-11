#pragma once

#include <sstream>
#include <string>
#include <vector>
#include <iostream>

#include <lua.hpp>

#define LUAPP11_DEBUG_HALT() { __debugbreak(); }

#define LUAPP11_DEBUG_HALT_IF(Condition) { if ((Condition)) LUAPP11_DEBUG_HALT() }

#ifdef LUAPP11_DEBUG

#define LUAPP11_DEBUG_STACK_DUMP(L) { std::cout << "Stack dump: " << luapp11::detail::GetStackDump(L) << std::endl; }
#define LUAPP11_DEBUG_TABLE_DUMP(L, Index) { std::cout << "Table @ " << lua_absindex(L, Index) << ": " << luapp11::detail::GetTableDump(L, Index) << std::endl; }

#else

#define LUAPP11_DEBUG_STACK_DUMP(L)
#define LUAPP11_DEBUG_TABLE_DUMP(L, Index)

#endif

namespace luapp11
{

	namespace detail
	{

		std::string GetStackDump(lua_State *L);
		std::string GetTableDump(lua_State *L, int index);

	}

}