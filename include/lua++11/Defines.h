#pragma once

#include <lua.hpp>

#define LUAPP11_OVERLOADTABLE_LUACFUNCTION   (-1)

#define LUAPP11_FUNCTIONTABLE_FUNCTION       (1)
#define LUAPP11_FUNCTIONTABLE_RETURNVALUES   (2)
#define LUAPP11_FUNCTIONTABLE_SIGNATURE      (3)
#define LUAPP11_FUNCTIONTABLE_ARGS           (4)

#define LUAPP11_CONSTRUCTORTABLE_CONSTRUCTOR (1)
#define LUAPP11_CONSTRUCTORTABLE_SIGNATURE   (2)
#define LUAPP11_CONSTRUCTORTABLE_ARGS        (3)

#define LUAPP11_REGKEY_CLASSREGISTRY         "__lpp11_cr"

#define LUAPP11_METATABLE_OVERLOAD           "__lpp11_omt"
#define LUAPP11_METATABLE_CONSTRUCTOR        "__lpp11_cmt"

#define LUAPP11_CLASSMT_CLASSID              (1)
#define LUAPP11_CLASSMT_CONSTRUCTORS         (2)
#define LUAPP11_CLASSMT_PROPERTIES           (3)
#define LUAPP11_CLASSMT_CASTERS              (4)
#define LUAPP11_CLASSMT_CUSTOMINDEX          (5)
#define LUAPP11_CLASSMT_CUSTOMNEWINDEX       (6)

#define LUAPP11_POINTER_METATABLE            "__lpp11_ptmt"
#define LUAPP11_PROPERTY_METATABLE           "__lpp11_prmt"
#define LUAPP11_ENUM_METATABLE               "__lpp11_emt"

#define LUAPP11_PROPERTY_CGET                (1)
#define LUAPP11_PROPERTY_GET                 (2)
#define LUAPP11_PROPERTY_SET                 (3)

#define LUAPP11_CLASSID_INVALID              (-1)

#define LUAPP11_INLINE                       inline