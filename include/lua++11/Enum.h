#pragma once

#include "Registrable.h"
#include "RegistrationTree.h"

namespace luapp11
{

	class IEnum :
		public Registrable
	{

	public:

		LUAPP11_INLINE RegistrationFunctionType GetRegistrationFunction(void)
		{
			return m_function;
		}

	protected:

		RegistrationFunctionType m_function;

	};

	template <typename T>
	class Enum :
		public IEnum
	{

	public:

		Enum(const char *name) :
			m_name(name)
		{
			m_type = ENUM;
			m_function = std::bind(&detail::RegistrationTree::RegisterEnum<T>, std::placeholders::_1, m_name, &m_enum);
		}

		Enum& operator() (const char *label, T value)
		{
			m_enum[label] = static_cast<lua_Integer>(value);
			return *this;
		}

	private:

		std::unordered_map<std::string, lua_Integer> m_enum;
		std::string                                  m_name;

	};

}