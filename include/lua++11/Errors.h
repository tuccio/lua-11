#pragma once

#ifdef LUAPP11_WINDOWS
#include "Windows.h"
#else
#include <iostream>
#endif

#include <lua.hpp>
#include "Defines.h"

namespace luapp11
{

#ifdef LUAPP11_WINDOWS

	LUAPP11_INLINE void NotifyError(const char *error)
	{
#ifdef LUAPP11_VERBOSE
		MessageBox(NULL, error, "Lua error", MB_OK | MB_ICONERROR);
#endif
	}

#else

	LUAPP11_INLINE void NotifyError(const char *error)
	{
#ifdef LUAPP11_VERBOSE
		std::cout << error << std::endl;
#endif
	}

#endif

	int LuaErrorHandler(lua_State *L);

}