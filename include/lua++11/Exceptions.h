#pragma once

#include <exception>

namespace luapp11
{

	struct LuaException :
		public std::exception
	{

		LuaException(void) :
			std::exception() { }

		LuaException(const char *msg) :
			std::exception(msg) { }

	};

	struct NilValueException :
		LuaException
	{

		NilValueException(void) { }

		NilValueException(const char *msg) : LuaException(msg) { }

	};

}