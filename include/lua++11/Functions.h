#pragma once

#include <functional>
#include <sstream>
#include <tuple>
#include <type_traits>
#include <typeindex>

#include <lua.hpp>

#include "Bind.h"
#include "Defines.h"
#include "Helpers.h"
#include "RegistrationForwardDeclarations.h"
#include "Registrable.h"
#include "RegistrationTree.h"
#include "Signature.h"


namespace luapp11
{

	template <typename R, typename ... Args>
	struct ConcreteCaller
	{

		/*template <typename Function>
		R && Call(Funciton f, Args && ... args)
		{
			return std::forward<R>(f(std::forward<Args>(args) ...));
		}*/

		template <typename Function, typename Tuple, int ... N>
		static R Call(Function f, Tuple && args, detail::IntegerSequence<N ...>)
		{
			return f(std::forward<Args>(std::get<N>(args)) ...);
		}

	};

	template <typename R, typename Function, typename Tuple, int ... N>
	typename std::enable_if<!std::is_void<R>::value, R &&>::type
		CppFunctionConcreteCaller(Function & f, Tuple && args, detail::IntegerSequence<N ...>)
	{
		return std::forward <R> ( f ( std::forward < ( typename std::tuple_element<N, Tuple>::type ) > ( std::get<N>(args) ) ... ) );
	}

	template <typename R, typename Function, typename Tuple, int ... N>
	typename std::enable_if<std::is_void<R>::value, void>::type CppFunctionConcreteCaller(Function & f, Tuple && args, detail::IntegerSequence<N ...>)
	{
		f(std::get<N>(args) ...);
	}

	template <typename R, typename Function, typename ... Types, int ... N>
	typename R CppFunctionConcreteCaller(Function & f, Types && ... args, detail::IntegerSequence<N ...>)
	{
		return f(std::get<N>(args) ...);
	}

	template <typename R, typename ... Args>
	typename std::enable_if<std::is_void<R>::value, int>::type CppFunctionCaller(lua_State *L)
	{

		typedef detail::LuaToCppTupleConverter<Args ...> TupleConverterType;
		auto sequence = detail::IntegerSequenceGenerator<sizeof ... (Args)>::type();

		std::function<R(Args ...)> *f = static_cast<std::function<R(Args ...)>*>(lua_touserdata(L, lua_upvalueindex(1)));

		if (detail::LuaToCppTupleConverter<Args ...>::IsConvertible(L, sequence))
		{
			auto args = detail::LuaToCppTupleConverter<Args ...>::Apply(L, sequence);
			CppFunctionConcreteCaller<R>(*f, args, sequence);
		}

		else
		{
			std::stringstream ss;
			ss << "Objects on stack do not match function arguments for function " << std::type_index(typeid(R(Args ...))).name() << std::endl;
			luaL_error(L, ss.str().c_str());
		}

		return 0;

	}

	template <typename R, typename ... Args>
	typename std::enable_if<!std::is_void<R>::value, int>::type CppFunctionCaller(lua_State *L)
	{

		auto sequence = detail::IntegerSequenceGenerator<sizeof ... (Args)>::type();

		std::function<R(Args ...)> *f = static_cast<std::function<R(Args ...)>*>(lua_touserdata(L, lua_upvalueindex(1)));

		if (detail::LuaToCppTupleConverter<Args ...>::IsConvertible(L, sequence))
		{
			auto args = detail::LuaToCppTupleConverter<Args ...>::Apply(L, sequence);
			//CppToLuaConverter<R>::Apply(L, std::forward<R>(CppFunctionConcreteCaller<R>(*f, args, sequence)));

			CppToLuaConverter<R>::Apply(L, std::forward<R> ( ConcreteCaller<R, Args...>::Call(*f, args, sequence) ) );
			return 1;
		}

		else
		{
			std::stringstream ss;
			ss << "Objects on stack do not match function arguments for function " << std::type_index(typeid(R(Args ...))).name() << std::endl;
			luaL_error(L, ss.str().c_str());
		}

		return 0;

	}

	namespace detail
	{

		template <typename R, typename ... Args>
		LUAPP11_INLINE lua_CFunction MakeFunctionCppCaller(std::function < R(Args ...)> f)
		{
			return &CppFunctionCaller < R, Args ... > ;
		}

	}

	/* Registration stuff */

	class IFunctionInfo :
		public Registrable
	{

	public:

		const char* GetName(void) const
		{
			return m_name.c_str();
		}

		RegistrationFunctionType GetRegistrationFunction(void)
		{
			return m_function;
		}

	protected:

		std::string              m_name;
		RegistrationFunctionType m_function;


	};	

	template <typename R, typename ... Args>
	struct FunctionInfo<R, Args ...> :
		public IFunctionInfo
	{

		FunctionInfo(void)
		{

		}

		FunctionInfo(const char *name, std::function<R(Args ...)> f)
		{
			m_name     = name;
			m_function = std::bind((&detail::RegistrationTree::RegisterFunction<R, Args...>), std::placeholders::_1, name, f);
			m_type     = FUNCTION;
		}

	};

	template <>
	struct FunctionInfo<int, lua_State *> :
		public IFunctionInfo
	{

		FunctionInfo(void)
		{

		}

		FunctionInfo(const char *name, lua_CFunction f)
		{
			m_name     = name;
			m_function = std::bind(&detail::RegistrationTree::RegisterLuaFunction, std::placeholders::_1, name, f);
			m_type     = FUNCTION;
		}

	};	

	namespace detail
	{

		LUAPP11_INLINE lua_CFunction MakeFunction(lua_CFunction f)
		{
			return f;
		}

		template <typename R, typename ... Args>
		LUAPP11_INLINE std::function<R(Args ...)> MakeFunction(R (*f) (Args ...))
		{
			return std::function<R(Args ...)>(f);
		}

		template <typename R, typename ... Args>
		LUAPP11_INLINE std::function<R(Args ...)> MakeFunction(std::function<R(Args ...)> f)
		{
			return f;
		}

		template <typename R, typename C, typename ... Args>
		LUAPP11_INLINE std::function<R(C*, Args ...)> MakeFunction(R (C::*f) (Args ...))
		{
			return detail::Bind(f, detail::IntegerSequenceGenerator<sizeof ... (Args) + 1>::type());
		}

		template <typename R, typename C, typename ... Args>
		LUAPP11_INLINE std::function<R(const C*, Args ...)> MakeFunction(R (C::*f) (Args ...) const)
		{
			return detail::Bind(f, detail::IntegerSequenceGenerator<sizeof ... (Args) + 1>::type());
		}

	}
	
	template <typename R, typename ... Args>
	LUAPP11_INLINE FunctionInfo<R, Args ...> Function(const char *name, R(*f) (Args ...))
	{
		return FunctionInfo<R, Args ...>(name, detail::MakeFunction(f));
	}

	template <typename R, typename ... Args>
	LUAPP11_INLINE FunctionInfo<R, Args ...> Function(const char *name, std::function<R(Args ...)> f)
	{
		return FunctionInfo<R, Args ...>(name, f);
	}

	template <typename R, typename C, typename ... Args>
	LUAPP11_INLINE FunctionInfo<R, C*, Args ...> Function(const char *name, R(C::*f) (Args ...))
	{
		return FunctionInfo<R, C*, Args ...>(name, detail::Bind(f, detail::IntegerSequenceGenerator<sizeof ... (Args)+1>::type()));
	}

	template <typename R, typename C, typename ... Args>
	LUAPP11_INLINE FunctionInfo<R, const C*, Args ...> Function(const char *name, R(C::*f) (Args ...) const)
	{
		return FunctionInfo<R, const C*, Args ...>(name, detail::Bind(f, detail::IntegerSequenceGenerator<sizeof ... (Args)+1>::type()));
	}

}