#pragma once

#include <lua.hpp>

namespace luapp11
{
	
	class StackCleaner
	{

	public:

		StackCleaner(lua_State *L) : m_lua(L), m_top(lua_gettop(L)) { }
		~StackCleaner(void) { lua_settop(m_lua, m_top); }

	private:

		lua_State *m_lua;
		int        m_top;

	};

}