#pragma once

#include "RegistryReference.h"

namespace luapp11
{

	class LuaBehaviour
	{

	public:

		LuaBehaviour(void);
		~LuaBehaviour(void);

		bool Create(lua_State *L, const char *filename, bool allowGlobalWrite = false);
		void Destroy(void);

		LUAPP11_INLINE lua_State * GetState(void) const
		{
			return m_ref.GetState();
		}

		LUAPP11_INLINE operator bool () { return (bool) m_ref; }

	private:

		RegistryReference m_ref;

		friend void Push(const LuaBehaviour &b);

	};

	LUAPP11_INLINE void Push(const LuaBehaviour &b)
	{
		Push(b.m_ref);
	}

}

