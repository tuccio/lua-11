#pragma once

#include <lua.hpp>

#include <type_traits>
#include <string>

#include "ClassRegistry.h"
#include "Debug.h"
#include "Defines.h"
#include "Helpers.h"
#include "Types.h"

namespace luapp11
{

	template <typename T, typename Enable = void>
	struct LuaToCppConverter;

	template <>
	struct LuaToCppConverter<void>
	{

		static bool IsConvertible(lua_State *L, int index) { return false; }
		static void Apply(lua_State *L, int index) { }

	};

	/* Bool */

	template <>
	struct LuaToCppConverter<bool>
	{

		static bool IsConvertible(lua_State *L, int index)
		{
			return lua_isboolean(L, index);
		}

		static bool Apply(lua_State *L, int index)
		{
			return lua_toboolean(L, index) != 0;
		}

	};

	/* Floating numbers */

	template <typename T>
	struct LuaToCppConverter<T, typename std::enable_if<std::is_floating_point<T>::value>::type>
	{

		static bool IsConvertible(lua_State *L, int index)
		{
			return lua_type(L, index);
		}

		static T Apply(lua_State *L, int index)
		{
			return T(lua_tonumber(L,index));
		}

	};

	template <typename T>
	struct LuaToCppConverter<const T, typename std::enable_if<std::is_floating_point<T>::value>::type> :
		public LuaToCppConverter<T>{};

	template <typename T>
	struct LuaToCppConverter<const T&, typename std::enable_if<std::is_floating_point<T>::value>::type> :
		public LuaToCppConverter<T>{};

	/* Integral numbers */

	template <typename T>
	struct LuaToCppConverter<T, typename std::enable_if<std::is_integral<T>::value>::type>
	{

		static bool IsConvertible(lua_State *L, int index)
		{
			return lua_type(L, index) == LUA_TNUMBER;
		}

		static T Apply(lua_State *L, int index)
		{
			return T(lua_tointeger(L, index));
		}

	};

	template <typename T>
	struct LuaToCppConverter<const T, typename std::enable_if<std::is_integral<T>::value>::type> :
		public LuaToCppConverter<T>{};

	template <typename T>
	struct LuaToCppConverter<const T&, typename std::enable_if<std::is_integral<T>::value>::type> :
		public LuaToCppConverter<T>{};

	/* Enums */

	template <typename T>
	struct LuaToCppConverter < T, typename std::enable_if<std::is_enum<T>::value>::type >
	{

		static bool IsConvertible(lua_State *L, int index)
		{
			return lua_type(L, index) == LUA_TNUMBER;
		}

		static T Apply(lua_State *L, int index)
		{
			return T(lua_tointeger(L, index));
		}

	private:

		lua_State *m_lua;
		int        m_index;

	};

	template <typename T>
	struct LuaToCppConverter < const T, typename std::enable_if<std::is_enum<T>::value>::type > :
		public LuaToCppConverter<T> { };

	/* Strings */

	template <>
	struct LuaToCppConverter<const char *>
	{

		static bool IsConvertible(lua_State *L, int index)
		{
			return lua_type(L, index) == LUA_TSTRING;
		}

		static const char * Apply(lua_State *L, int index)
		{
			return lua_tostring(L, index);
		}

	};

	template <>
	struct LuaToCppConverter<std::string>
	{

		static bool IsConvertible(lua_State *L, int index)
		{
			return lua_type(L, index) == LUA_TSTRING;
		}

		static std::string Apply(lua_State *L, int index)
		{
			return std::string(lua_tostring(L, index));
		}

	};

	template <>
	struct LuaToCppConverter<const std::string> :
		public LuaToCppConverter<std::string> { };

	template <>
	struct LuaToCppConverter<const std::string &> :
		public LuaToCppConverter<std::string> { };

	/* Shared pointers */

	template <typename T>
	struct LuaToCppConverter<std::shared_ptr<T>, typename std::enable_if<std::is_class<T>::value>::type>
	{

		static bool IsConvertible(lua_State *L, int index)
		{

			bool result = false;

			if(lua_type(L, index) == LUA_TUSERDATA)
			{

				detail::LuaObjectWrapper *o = static_cast<detail::LuaObjectWrapper*>(lua_touserdata(L, index));

				if (!o->IsEmpty())
				{

					detail::ClassID        cid = o->GetClassID();
					detail::ClassRegistry *cr  = detail::ClassRegistry::GetInstance(L);

					result = cr->GetPathToBase(detail::RTTI<T>::id, cid);

				}

			}

			return result;

		}

		static std::shared_ptr<T> Apply(lua_State *L, int index)
		{

			detail::LuaObjectWrapper *o = static_cast<detail::LuaObjectWrapper*>(lua_touserdata(L, index));
			return o->Upcast<T>(L, detail::RTTI<T>::id).GetSharedPtr<T>();

		}

	};

	/* Registered classes */

	template <typename T>
	struct LuaToCppConverter<T*, typename std::enable_if<std::is_class<T>::value>::type>
	{

		static bool IsConvertible(lua_State *L, int index)
		{

			bool result = false;

			if (lua_type(L, index) == LUA_TUSERDATA)
			{

				detail::LuaObjectWrapper *o = static_cast<detail::LuaObjectWrapper*>(lua_touserdata(L, index));

				if (!o->IsEmpty() && !o->IsConst())
				{

					detail::ClassID        cid = o->GetClassID();
					detail::ClassRegistry *cr  = detail::ClassRegistry::GetInstance(L);

					result = cr->GetPathToBase(detail::RTTI<T>::id, cid);

				}

			}

			return result;

		}

		static T * Apply(lua_State *L, int index)
		{

			detail::LuaObjectWrapper *o = static_cast<detail::LuaObjectWrapper*>(lua_touserdata(L, index));
			detail::LuaObjectWrapper castedObject = o->Upcast<T>(L, detail::RTTI<T>::id);
			return castedObject.GetPointer<T>();

		}

	};

	template <typename T>
	struct LuaToCppConverter<const T *, typename std::enable_if<std::is_class<T>::value>::type>
	{

		static bool IsConvertible(lua_State *L, int index)
		{

			bool result = false;

			if (lua_type(L, index) == LUA_TUSERDATA)
			{

				detail::LuaObjectWrapper *o = static_cast<detail::LuaObjectWrapper*>(lua_touserdata(L, index));

				if (!o->IsEmpty())
				{

					detail::ClassID        cid = o->GetClassID();
					detail::ClassRegistry *cr  = detail::ClassRegistry::GetInstance(L);

					result = cr->GetPathToBase(detail::RTTI<T>::id, cid);

				}

			}

			return result;

		}

		static const T * Apply(lua_State *L, int index)
		{

			detail::LuaObjectWrapper *o = static_cast<detail::LuaObjectWrapper*>(lua_touserdata(L, index));
			detail::LuaObjectWrapper castedObject = o->Upcast<T>(L, detail::RTTI<T>::id);
			return castedObject.GetPointer<const T>();

		}

	};

	template <typename T>
	struct LuaToCppConverter <T, typename std::enable_if<std::is_class<T>::value>::type >
	{

		static bool IsConvertible(lua_State *L, int index)
		{
			return LuaToCppConverter<const T*>::IsConvertible(L, index);
		}

		static T Apply(lua_State *L, int index)
		{
			return * LuaToCppConverter<const T*>::Apply(L, index);
		}

	};

	template <typename T>
	struct LuaToCppConverter <T&, typename std::enable_if<std::is_class<T>::value>::type >
	{

		static bool IsConvertible(lua_State *L, int index)
		{
			return LuaToCppConverter<T*>::IsConvertible(L, index);
		}

		static T & Apply(lua_State *L, int index)
		{
			return * LuaToCppConverter<T*>::Apply(L, index);
		}

	};

	namespace detail
	{

		template <typename T, int Index>
		struct LuaToCppFixedConverter :
			public LuaToCppConverter<T>
		{

			LuaToCppFixedConverter(lua_State *L) :
				LuaToCppConverter<T>(L, Index)
			{
			}

		};

		template <int ... N>
		struct LogicEvaluator;

		template <>
		struct LogicEvaluator<>
		{

			template <typename Tuple>
			static bool And(Tuple && t)
			{
				return true;
			}

		};

		template <int First, int ... N>
		struct LogicEvaluator<First, N ...>
		{

			template <typename Tuple>
			static bool And(Tuple && t)
			{
				return std::get<First>(t) && LogicEvaluator<N ...>::And(t);
			}

		};

		// Inconsistent: checks the 2 ... N + 2 args on stack but retrieves the - N ... - 1


		template <typename ... Types>
		struct LuaToCppTupleConverter
		{

			template <int ... N>
			static bool IsConvertible(lua_State *L, IntegerSequence<N ...>)
			{
				//LUAPP11_DEBUG_STACK_DUMP(L);
				return LogicEvaluator<N ...>::And(std::make_tuple(LuaToCppConverter<Types>::IsConvertible(L, - (int) sizeof ... (Types) + N) ...));
			}

			template <int ... N>
			static auto Apply(lua_State *L, IntegerSequence<N ...>) -> std::tuple < decltype( LuaToCppConverter<Types>::Apply(L, 0) ) ... >
			{
				return std::tuple < decltype(LuaToCppConverter<Types>::Apply(L, - (int) sizeof ... (Types) + N)) ... >(LuaToCppConverter<Types>::Apply(L, - (int) sizeof ... (Types) + N) ...);
			}

		};

		template <>
		struct LuaToCppTupleConverter<>
		{

			template <int ... N>
			static bool IsConvertible(lua_State *L, IntegerSequence<N ...>)
			{
				//LUAPP11_DEBUG_STACK_DUMP(L);
				return true;
			}

			template <int ... N>
			static std::tuple<> Apply(lua_State *L, IntegerSequence<N ...>)
			{
				return std::tuple<>();
			}

		};

	}

	

}