#pragma once

#include "Registrable.h"
#include "RegistrationTree.h"

namespace luapp11
{

	class Namespace :
		public Registrable
	{

	public:

		Namespace(void);
		Namespace(const char *name);

		const char* GetName(void) const;
		RegistrationFunctionType GetRegistrationFunction(void);

		Namespace& operator[] (detail::RegistrationTree & tree);
		Namespace& operator[] (Registrable & r);


	private:

		friend class State;
		friend class detail::RegistrationTree;

		std::string              m_name;
		detail::RegistrationTree m_tree;
		RegistrationFunctionType m_function;

	};
}