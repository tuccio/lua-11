#pragma once

#include <lua.hpp>

#include "Defines.h"
#include "Exceptions.h"
#include "RegistryReference.h"

#include "Converters.h"
#include "LuaBehaviour.h"

namespace luapp11
{
	
	struct FromStack
	{
		
		FromStack(lua_State *L, int index) :
			L(L),
			index(index)
		{
		}

		lua_State *L;
		int index;
	};

	class TableField;

	template <typename T>
	class Optional
	{

	public:

		Optional(void) : m_initialized(false) { }
		
		template <typename U>
		Optional(U v) : m_initialized(true) { new (m_storage) (std::forward<T>(v)); }	

		LUAPP11_INLINE operator bool (void) { return m_initialized; }

		LUAPP11_INLINE T & Get(void) { return * reinterpret_cast<T *>(m_storage); }
		LUAPP11_INLINE const T & Get(void) { return * reinterpret_cast<const T *>(m_storage); }

	private:

		bool m_initialized;
		std::aligned_storage_t<sizeof(T), std::alignment_of<T>::value> m_storage;

	};

	class Object
	{

	public:

		Object(void);
		Object(const FromStack &fs);
		Object(Object &object, const char *field);

		Object(Object && object);

		template <typename T>
		Object(lua_State *L, T *object);

		template <typename T>
		Object(lua_State *L, T && object);

		template <typename T>
		Object(lua_State *L, const T& object);

		void Push(void) const;

		LUAPP11_INLINE lua_State* GetState(void) const;
		LUAPP11_INLINE int        GetLuaType(void) const;

		void SetBehaviour(const LuaBehaviour &b);

		template <typename R, typename ... Args>
		R Call(const char *f, Args && ... args);

		template <typename R, typename ... Args>
		Optional<R> TryCall(const char *f, Args && ... args);

		template <typename R, typename ... Args>
		R Call(Args && ... args);

		template <typename T>
		T Cast(void);

		template <typename T>
		bool IsCastable(void);

		TableField operator[] (const char *field);

		LUAPP11_INLINE operator bool() { return m_ref.get() && *m_ref; }

	protected:

		SharedRegistryReference m_ref;
		int                     m_luaType;
		detail::ClassID         m_cid;

		friend class TableField;

	};

	class TableField :
		public Object
	{

	public:

		template <typename T>
		TableField& operator= (T && value);

	private:

		TableField(Object &object, const char *field);
		
		Object      m_parent;
		const char *m_field;

		friend class Object;

	};

	/* Object template functions implementations */

	template <typename T>
	Object::Object(lua_State *L, T *object)
	{

		CppToLuaConverter<T*>::Apply(L, object);
		RegistryReference ref;
		ref.Create(L, -1);

		m_ref = MakeSharedReference(std::move(ref));

		if (std::is_class<luapp11::detail::RemoveQualifiers<T>>::value)
		{
			m_luaType = LUA_TUSERDATA;
			m_cid = detail::RTTI<T>::id;
		}
		else
		{
			m_luaType = lua_type(L, -1);
		}

		lua_pop(L, 1);

	}

	template <typename T>
	Object::Object(lua_State *L, T && object)
	{

		CppToLuaConverter<T>::Apply(L, std::forward<T>(object));
		RegistryReference ref;
		ref.Create(L, -1);

		m_ref = MakeSharedReference(std::move(ref));

		if (std::is_class<luapp11::detail::RemoveQualifiers<T>::type>::value)
		{
			m_luaType = LUA_TUSERDATA;
			m_cid = detail::RTTI<T>::id;
		}
		else
		{
			m_luaType = lua_type(L, -1);
		}

		lua_pop(L, 1);

	}

	template <typename T>
	Object::Object(lua_State *L, const T & object)
	{

		CppToLuaConverter<T>::Apply(L, std::forward<T>(object));
		RegistryReference ref;
		ref.Create(L, -1);

		m_ref = MakeSharedReference(std::move(ref));

		if (std::is_class<luapp11::detail::RemoveQualifiers<T>>::value)
		{
			m_luaType = LUA_TUSERDATA;
			m_cid = detail::RTTI<T>::id;
		}
		else
		{
			m_luaType = lua_type(L, -1);
		}

		lua_pop(L, 1);

	}

	template <typename R, typename ... Args>
	R Object::Call(const char *f, Args && ... args)
	{

		lua_State *L = m_ref->GetState();
		StackCleaner sc(L);

		Push();
		lua_getmetatable(L, -1);
		lua_pushstring(L, "__index");
		lua_rawget(L, -2);

		Push();
		lua_pushstring(L, f);
		lua_call(L, 2, 1);

		if (lua_isnil(L, -1))
		{
			throw NilValueException("Attempting to call a nil value");
		}

		Push();

		detail::CppToLuaTupleConverter<Args ...>::Apply(L, std::forward<Args>(args) ...);
		lua_call(L, sizeof ... (Args) + 1, std::is_void<R>::value ? 0 : 1);

		return Object(FromStack(L, -1)).Cast<R>();

	}

	template <typename R, typename ... Args>
	Optional<R> Object::TryCall(const char *f, Args && ... args)
	{

		lua_State *L = m_ref->GetState();
		StackCleaner sc(L);

		Push();
		lua_getmetatable(L, -1);
		lua_pushstring(L, "__index");
		lua_rawget(L, -2);

		Push();
		lua_pushstring(L, f);
		lua_call(L, 2, 1);

		if (lua_isnil(L, -1))
		{
			return Optional<R>();
		}

		Push();

		detail::CppToLuaTupleConverter<Args ...>::Apply(L, std::forward<Args>(args) ...);
		lua_call(L, sizeof ... (Args) +1, std::is_void<R>::value ? 0 : 1);

		return Optional<R>(std::forward<R>(Object(FromStack(L, -1)).Cast<R>()));

	}

	template <typename R, typename ... Args>
	R Object::Call(Args && ... args)
	{

		lua_State *L = m_ref->GetState();
		StackCleaner sc(L);

		Push();

		detail::CppToLuaTupleConverter<Args ...>::Apply(L, std::forward<Args>(args) ...);
		lua_call(L, sizeof ... (Args), std::is_void<R>::value ? 0 : 1);

		return Object(FromStack(L, -1)).Cast<R>();

	}

	template <typename T>
	T Object::Cast(void)
	{
		lua_State *L = GetState();
		StackCleaner cleaner(L);
		Push();
		return LuaToCppConverter<T>::Apply(L, -1);
	}

	template <typename T>
	bool Object::IsCastable(void)
	{
		lua_State *L = GetState();
		StackCleaner cleaner(L);
		Push();
		return LuaToCppConverter<T>::IsConvertible(L, -1);
	}

	lua_State * Object::GetState(void) const
	{
		return m_ref->GetState();
	}

	int Object::GetLuaType(void) const
	{
		return m_luaType;
	}

	/* TableField template functions implementation */

	template <typename T>
	TableField& TableField::operator= (T && value)
	{

		lua_State *L = GetState();
		StackCleaner sc(L);

		Object newValue = Object(L, std::forward<T>(value));

		m_parent.Push();
		newValue.Push();
		lua_setfield(L, -2, m_field);

		m_ref     = newValue.m_ref;
		m_luaType = newValue.m_luaType;
		m_cid     = newValue.m_cid;

		return *this;

	}

	LUAPP11_INLINE Object CreateTable(lua_State * L)
	{
		lua_newtable(L);
		return Object(FromStack(L, -1));
	}

	/* Converters */

	template <>
	struct LuaToCppConverter<Object>
	{

		static bool IsConvertible(lua_State *L, int index)
		{
			return true;
		}

		static Object Apply(lua_State *L, int index)
		{
			return Object(FromStack(L, index));
		}

	};

	template <>
	struct CppToLuaConverter<Object>
	{

		static void Apply(lua_State *L, const Object & value)
		{
			assert(L == value.GetState());
			value.Push();
		}

	};

	template <>
	struct CppToLuaConverter<TableField>
	{

		static void Apply(lua_State * L, const TableField & value)
		{
			assert(L == value.GetState());
			value.Push();
		}

	};

}