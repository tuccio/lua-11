#pragma once

#include <functional>
#include <type_traits>
#include <utility>

#include "Defines.h"

namespace luapp11
{

	template <typename T>
	struct Operand
	{
		typedef T type;
	};

	enum Operators
	{

		/* Binary operators */

		LUAPP11_OPERATOR_ADD,
		LUAPP11_OPERATOR_SUB,
		LUAPP11_OPERATOR_MUL,
		LUAPP11_OPERATOR_DIV,

		LUAPP11_OPERATOR_MOD,
		LUAPP11_OPERATOR_POW,
		LUAPP11_OPERATOR_IDIV,


		LUAPP11_OPERATOR_BAND,
		LUAPP11_OPERATOR_BOR,
		LUAPP11_OPERATOR_BXOR,

		LUAPP11_OPERATOR_SHL,
		LUAPP11_OPERATOR_SHR,

		LUAPP11_OPERATOR_CONCAT,

		LUAPP11_OPERATOR_EQ,
		LUAPP11_OPERATOR_LE,
		LUAPP11_OPERATOR_LT,

		/* Unary operators */

		LUAPP11_OPERATOR_UNM,
		LUAPP11_OPERATOR_BNOT,
		LUAPP11_OPERATOR_LEN


	};

	template <typename T1, typename T2, Operators op>
	struct BinaryOperation { };

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_ADD> operator+(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_ADD>();
	}

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_SUB> operator-(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_SUB>();
	}

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_MUL> operator*(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_MUL>();
	}

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_DIV> operator/(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_DIV>();
	}

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_MOD> operator%(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_MOD>();
	}

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_SHL> operator<<(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_SHL>();
	}

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_SHR> operator>>(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_SHR>();
	}

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_EQ> operator==(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_EQ>();
	}

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_LT> operator<(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_LT>();
	}

	template <typename T1, typename T2>
	BinaryOperation<T1, T2, LUAPP11_OPERATOR_LE> operator<=(Operand<T1> &&, Operand<T2> &&)
	{
		return BinaryOperation<T1, T2, LUAPP11_OPERATOR_LE>();
	}

	template <typename T, Operators op>
	struct UnaryOperation { };

	template <typename T>
	UnaryOperation<T, LUAPP11_OPERATOR_UNM> operator-(Operand<T> &&)
	{
		return UnaryOperation<T, LUAPP11_OPERATOR_UNM>();
	}

	template <typename T>
	UnaryOperation<T, LUAPP11_OPERATOR_BNOT> operator~(Operand<T> &&)
	{
		return UnaryOperation<T, LUAPP11_OPERATOR_BNOT>();
	}

	template <Operators op>
	struct OperatorsWrappersGenerator;

	/* Addition */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_ADD >
	{

		static const char * metafunction;

		template <typename T1, typename T2>
		static LUAPP11_INLINE auto OperatorWrapper(T1 a, T2 b) -> decltype(a + b)
		{
			return a + b;
		}

	};

	/* Subtraction */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_SUB >
	{

		static const char * metafunction;

		template <typename T1, typename T2>
		static LUAPP11_INLINE auto OperatorWrapper(T1 a, T2 b) -> decltype(a - b)
		{
			return a - b;
		}

	};

	/* Multiplication */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_MUL >
	{

		static const char* metafunction;

		template <typename T1, typename T2>
		static LUAPP11_INLINE auto OperatorWrapper(T1 a, T2 b) -> decltype(a * b)
		{
			return a * b;
		}

	};

	/* Division */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_DIV >
	{

		static const char * metafunction;

		template <typename T1, typename T2>
		static auto OperatorWrapper(T1 a, T2 b) -> decltype(a / b)
		{
			return a / b;
		}

	};

	/* Module */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_MOD >
	{

		static const char * metafunction;

		template <typename T1, typename T2>
		static auto OperatorWrapper(T1 a, T2 b) -> decltype(a % b)
		{
			return a % b;
		}

	};

	/* Left shift */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_SHL >
	{

		static const char * metafunction;

		template <typename T1, typename T2>
		static auto OperatorWrapper(T1 a, T2 b) -> decltype(a << b)
		{
			return a << b;
		}

	};

	/* Right shift */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_SHR >
	{

		static const char * metafunction;

		template <typename T1, typename T2>
		static auto OperatorWrapper(T1 a, T2 b) -> decltype(a >> b)
		{
			return a >> b;
		}

	};

	/* Equal */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_EQ >
	{

		static const char * metafunction;

		template <typename T1, typename T2>
		static auto OperatorWrapper(T1 a, T2 b) -> decltype(a == b)
		{
			return a == b;
		}

	};

	/* Less than */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_LT >
	{

		static const char * metafunction;

		template <typename T1, typename T2>
		static auto OperatorWrapper(T1 a, T2 b) -> decltype(a < b)
		{
			return a < b;
		}

	};

	/* Less equal */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_LE >
	{

		static const char * metafunction;

		template <typename T1, typename T2>
		static auto OperatorWrapper(T1 a, T2 b) -> decltype(a <= b)
		{
			return a <= b;
		}

	};

	/* Binary not (unary) */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_BNOT >
	{

		static const char * metafunction;

		template <typename T>
		static auto OperatorWrapper(T a, T) -> decltype(~a)
		{
			return ~a;
		}

	};

	/* Unary minus */

	template <>
	struct OperatorsWrappersGenerator < LUAPP11_OPERATOR_UNM >
	{

		static const char * metafunction;

		template <typename T>
		static auto OperatorWrapper(T a, T) -> decltype(-a)
		{
			return -a;
		}

	};



}