#pragma once

#include <lua.hpp>

#include <functional>

#include "Allocation.h"
#include "Defines.h"

namespace luapp11
{

	namespace detail
	{

		int OverloadChooser(lua_State *L);

		/* Function registrations */

		LUAPP11_INLINE void PushNewOverloadTable(lua_State *L)
		{

			lua_newtable(L);

			// Create the metatable

			if (luaL_newmetatable(L, LUAPP11_METATABLE_OVERLOAD))
			{

				// Call metafunction is OverloadChooser

				lua_pushstring(L, "__call");
				lua_pushcfunction(L, detail::OverloadChooser);
				lua_rawset(L, -3);

			}

			lua_setmetatable(L, -2);

		}

		template <typename R, typename ... Args>
		LUAPP11_INLINE void PushNewFunctionTable(lua_State *L, std::function<R(Args ...)> f)
		{

			// F[f] = CppFunctionCaller closure

			lua_newtable(L);

			new (L) std::function<R(Args ...)>(f);
			lua_pushcclosure(L, (&CppFunctionCaller<R, Args ...>), 1);

			lua_rawseti(L, -2, LUAPP11_FUNCTIONTABLE_FUNCTION);

			// F[r] = # return values

			int returnValues = std::is_void<R>::value ? 0 : 1;

			lua_pushinteger(L, returnValues);
			lua_rawseti(L, -2, LUAPP11_FUNCTIONTABLE_RETURNVALUES);

			lua_pushcfunction(L, (&SignatureCheck<R, Args ...>));
			lua_rawseti(L, -2, LUAPP11_FUNCTIONTABLE_SIGNATURE);

			lua_pushinteger(L, sizeof ... (Args));
			lua_rawseti(L, -2, LUAPP11_FUNCTIONTABLE_ARGS);

		}

	}

}