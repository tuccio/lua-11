#pragma once

namespace luapp11
{

	struct ConstReferencePolicy
	{

		template <typename T, typename C>
		static const T & Get(const C * object, T C::* pMember)
		{
			return object->*pMember;
		}

	};

	struct ReferencePolicy
	{

		template <typename T, typename C>
		static T & Get(C * object, T C::* pMember)
		{
			return object->*pMember;
		}

	};

}