#pragma once

namespace luapp11
{

	namespace detail
	{

		template <typename T>
		class Proxy
		{
			T *value;
		};

		template <typename T>
		struct IsProxy :
			public std::false_type { };

		template <typename T>
		struct IsProxy<Proxy<T>> :
			public std::true_type{};

	}

}