#pragma once

namespace luapp11
{

	class Registrable
	{

	public:

		enum Type
		{
			NAMESPACE,
			FUNCTION,
			CLASS,
			CONSTRUCTOR,
			PROPERTY,
			ENUM
		};

		Type GetType(void) const
		{
			return m_type;
		}

	protected:

		Type m_type;

	};

}