#pragma once

#include <lua.hpp>

#include <functional>
#include <string>
#include <vector>

/* Forward declarations */

namespace luapp11
{

	typedef std::function<void(lua_State *L)> RegistrationFunctionType;
	typedef lua_CFunction                     SignatureCheckType;

	class Namespace;

	template <typename C, typename ... BaseClasses>
	class Class;

	template <typename R, typename ... Args>
	struct FunctionInfo;

	namespace detail
	{

		class RegistrationTree;

		/*void RegisterTree(lua_State *L, RegistrationTree *rt);

		void RegisterNamespace(lua_State *L, std::string name, RegistrationTree *rt);

		template <typename C, typename ... BaseClasses>
		void RegisterCppClass(
			lua_State *L,
			std::string,
			Class<C, BaseClasses ...> *);

		void RegisterLuaFunction(lua_State *L, std::string name, lua_CFunction f);

		template <typename R, typename ... Args>
		void RegisterFunction(lua_State *L, std::string name, std::function<R(Args ...)> f);

		template <typename C>
		void RegisterConstructor(lua_State *L, SignatureCheckType sigcheck);

		template <typename R, typename C>
		void RegisterProperty(lua_State *L, std::string name, std::function<R(const C*)> get, std::function<void(C*, R)> set);*/

	}

}