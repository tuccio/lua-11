#pragma once

#include "ClassRegistry.h"
#include "Defines.h"
#include "RegistrationForwardDeclarations.h"
#include "Registrable.h"
#include "Wrapper.h"

#include <lua.hpp>

#include <string>

luapp11::detail::RegistrationTree& operator, (luapp11::detail::RegistrationTree &rt, luapp11::Registrable & r);
luapp11::detail::RegistrationTree  operator, (luapp11::Registrable & r1, luapp11::Registrable & r2);

namespace luapp11
{

	namespace detail
	{

		

		/* Registration Tree */

		class RegistrationTree
		{

		public:

			RegistrationTree(void)
			{

			}

			RegistrationTree(Registrable && r)
			{
				(*this, std::forward<Registrable>(r));
			}

			void Register(lua_State *L);

			static void RegisterNamespace(lua_State *L, std::string name, RegistrationTree *rt);

			template <typename C, typename ... BaseClasses>
			static void RegisterCppClass(
				lua_State *L,
				std::string,
				std::vector<RegistrationFunctionType> *constructors,
				std::vector<RegistrationFunctionType> *functions,
				std::vector<RegistrationFunctionType> *properties,
				lua_CFunction *customIndex,
				lua_CFunction *customNewIndex);

			static void RegisterLuaFunction(lua_State *L, std::string name, lua_CFunction f);

			template <typename R, typename ... Args>
			static void RegisterFunction(lua_State *L, std::string name, std::function<R(Args ...)> f);

			template <typename C>
			static void RegisterConstructor(lua_State *L, SignatureCheckType sigcheck);

			template <typename ConstGetter, typename Getter, typename Setter>
			static void RegisterProperty(lua_State *L, std::string name, ConstGetter cget, Getter get, Setter set);

			template <typename Getter, typename Setter>
			static void RegisterProperty(lua_State *L, std::string name, Getter get, Setter set);

			template <typename Getter>
			static void RegisterProperty(lua_State *L, std::string name, Getter get);

			template <typename T>
			static void RegisterEnum(lua_State *L, std::string name, std::unordered_map<std::string, lua_Integer> *e);


#define __LUAPP11_DETAIL_ATTACH_TREE(x, y) x.insert(x.end(), y.x.begin(), y.x.end())

			LUAPP11_INLINE RegistrationTree& Attach(RegistrationTree && rt)
			{

				__LUAPP11_DETAIL_ATTACH_TREE(m_functions, rt);
				__LUAPP11_DETAIL_ATTACH_TREE(m_namespaces, rt);
				__LUAPP11_DETAIL_ATTACH_TREE(m_classes, rt);
				__LUAPP11_DETAIL_ATTACH_TREE(m_constructors, rt);
				return *this;

			}

		private:

			std::vector<RegistrationFunctionType> m_functions;
			std::vector<RegistrationFunctionType> m_namespaces;
			std::vector<RegistrationFunctionType> m_classes;
			std::vector<RegistrationFunctionType> m_constructors;
			std::vector<RegistrationFunctionType> m_enums;

			friend RegistrationTree& ::operator, (RegistrationTree & rt, Registrable & r);

			template <typename T, typename ... BaseClasses>
			friend class Class;

		};

	}

}

#include "RegistrationTree.inl"