namespace luapp11
{

	namespace detail
	{

		template <typename R, typename ... Args>
		void RegistrationTree::RegisterFunction(lua_State *L, std::string name, std::function<R(Args ...)> f)
		{

			bool created = false;

			lua_pushstring(L, name.c_str());
			lua_rawget(L, -2);

			if (lua_isnil(L, -1))
			{

				lua_pop(L, 1);
				
				lua_pushstring(L, name.c_str());
				PushNewOverloadTable(L);

				created = true;

			}

			lua_len(L, -1);
			int len = lua_tointeger(L, -1);
			lua_pop(L, 1);

			/*PushNewFunctionTable(L, f);*/

			// F[f] = CppFunctionCaller closure

			// Function table begin

			lua_newtable(L);

			new (L) std::function<R(Args ...)>(f);
			lua_pushcclosure(L, (&CppFunctionCaller<R, Args ...>), 1);

			lua_rawseti(L, -2, LUAPP11_FUNCTIONTABLE_FUNCTION);

			// F[r] = # return values

			int returnValues = std::is_void<R>::value ? 0 : 1;

			lua_pushinteger(L, returnValues);
			lua_rawseti(L, -2, LUAPP11_FUNCTIONTABLE_RETURNVALUES);

			lua_pushcfunction(L, (&SignatureCheck<R, Args ...>));
			lua_rawseti(L, -2, LUAPP11_FUNCTIONTABLE_SIGNATURE);

			lua_pushinteger(L, sizeof ... (Args));
			lua_rawseti(L, -2, LUAPP11_FUNCTIONTABLE_ARGS);

			// Function table end

			lua_rawseti(L, -2, len + 1);

			if (created)
			{
				lua_rawset(L, -3);
			}
			else
			{
				lua_pop(L, 1);
			}

		}

		template <typename Class, typename ... BaseClasses>
		struct ClassCastsGenerator;

		template <typename Class>
		struct ClassCastsGenerator<Class>
		{

			static void Generate(ClassRegistryInfo *classReg)
			{

			}

		};

		template < typename Class, typename Base, typename ... BaseClasses >
		struct ClassCastsGenerator<Class, Base, BaseClasses ...>
		{

			static void Generate(ClassRegistryInfo *classReg)
			{

				classReg->upcastMap[RTTI<Base>::id]          = (UpcastFunction) &(Cast<Class, Base>);
				classReg->sharedptrUpcastMap[RTTI<Base>::id] = (UpcastFunction) &(Cast<std::shared_ptr<Class>, std::shared_ptr<Base>>);

				ClassCastsGenerator<Class, BaseClasses ...>::Generate(classReg);

			}

		};

		template <typename C, typename ... BaseClasses>
		void RegistrationTree::RegisterCppClass(
			lua_State *L,
			std::string name,
			std::vector<RegistrationFunctionType> *constructors,
			std::vector<RegistrationFunctionType> *functions,
			std::vector<RegistrationFunctionType> *properties,
			lua_CFunction *customIndex,
			lua_CFunction *customNewIndex)
		{

			bool created = false;

			ClassRegistry *cr = ClassRegistry::GetInstance(L);
			const char *className = name.c_str();

			ClassRegistryInfo *classReg = cr->Register<C>();

			if (classReg)
			{

				// Create the class metatable

				lua_pushstring(L, className);
				lua_newtable(L);

				classReg->name = name;
				classReg->cid  = RTTI<C>::id;
				classReg->metatable.Create(L, -1);

				ClassCastsGenerator<C, BaseClasses ...>::Generate(classReg);

				lua_pushvalue(L, -1);
				lua_setmetatable(L, -2);

				lua_pushinteger(L, RTTI<C>::id);
				lua_rawseti(L, -2, LUAPP11_CLASSMT_CLASSID);

				if (*customIndex)
				{
					lua_pushcfunction(L, *customIndex);
					lua_rawseti(L, -2, LUAPP11_CLASSMT_CUSTOMINDEX);
				}

				if (*customNewIndex)
				{
					lua_pushcfunction(L, *customNewIndex);
					lua_rawseti(L, -2, LUAPP11_CLASSMT_CUSTOMNEWINDEX);
				}

				lua_pushstring(L, "__index");
				lua_pushcfunction(L, &ClassIndexMetafunction);
				lua_rawset(L, -3);

				lua_pushstring(L, "__newindex");
				lua_pushcfunction(L, &ClassNewIndexMetafunction);
				lua_rawset(L, -3);

				lua_pushstring(L, "__gc");
				lua_pushcfunction(L, &ClassDestructor);
				lua_rawset(L, -3);

				lua_pushstring(L, "__call");

				// Constructor table

				lua_newtable(L);

				int i = 1;

				for (auto &ctor : *constructors)
				{
					ctor(L);
					lua_rawseti(L, -2, i++);
				}

				lua_pushcclosure(L, &detail::ConstructorChooser, 1);
				lua_rawset(L, -3);
				
				for (auto &f : *functions)
				{
					f(L);
				}

				// Properties table

				
				lua_newtable(L);

				for (auto &p : *properties)
				{
					p(L);
				}

				lua_rawseti(L, -2, LUAPP11_CLASSMT_PROPERTIES);

				lua_rawset(L, -3);

			}

			


		}

		template <typename C>
		void RegistrationTree::RegisterConstructor(lua_State *L, lua_CFunction sigcheck)
		{

			bool created = false;

			lua_pushstring(L, "__call");
			lua_rawget(L, -2);

			if (lua_isnil(L, -1))
			{

				lua_pop(L, 1);

				lua_pushstring(L, "__call");
				PushNewConstructorTable(L);
				created = true;

			}

			lua_len(L, -1);
			int len = lua_tointeger(L, -1);
			lua_pop(L, 1);

			PushNewConstructor<C>(L, sigcheck);
			lua_rawseti(L, -2, len + 1);

			if (created)
			{
				lua_rawset(L, -3);
			}
			else
			{
				lua_pop(L, 1);
			}

		}

		template <typename ConstGetter, typename Getter, typename Setter>
		void RegistrationTree::RegisterProperty(lua_State *L, std::string name, ConstGetter cget, Getter get, Setter set)
		{

			lua_pushstring(L, name.c_str());

			lua_newtable(L);

			auto bCGet = detail::MakeFunction(cget);

			new (L) decltype (bCGet) (bCGet);
			lua_pushcclosure(L, detail::MakeFunctionCppCaller(bCGet), 1);

			lua_rawseti(L, -2, LUAPP11_PROPERTY_CGET);

			auto bGet = detail::MakeFunction(get);

			new (L) decltype (bGet) (bGet);
			lua_pushcclosure(L, detail::MakeFunctionCppCaller(bGet), 1);

			lua_rawseti(L, -2, LUAPP11_PROPERTY_GET);

			auto bSet = detail::MakeFunction(set);

			new (L) decltype (bSet) (bSet);
			lua_pushcclosure(L, detail::MakeFunctionCppCaller(bSet), 1);

			lua_rawseti(L, -2, LUAPP11_PROPERTY_SET);

			luaL_newmetatable(L, LUAPP11_PROPERTY_METATABLE);
			lua_setmetatable(L, -2);

			lua_rawset(L, -3);

		}

		template <typename Getter, typename Setter>
		void RegistrationTree::RegisterProperty(lua_State *L, std::string name, Getter get, Setter set)
		{

			lua_pushstring(L, name.c_str());

			lua_newtable(L);

			auto bGet = detail::MakeFunction(get);

			new (L) decltype (bGet) (bGet);
			lua_pushcclosure(L, detail::MakeFunctionCppCaller(bGet), 1);

			lua_rawseti(L, -2, LUAPP11_PROPERTY_GET);

			lua_rawgeti(L, -1, LUAPP11_PROPERTY_GET);
			lua_rawseti(L, -2, LUAPP11_PROPERTY_CGET);

			auto bSet = detail::MakeFunction(set);

			new (L) decltype (bSet) (bSet);
			lua_pushcclosure(L, detail::MakeFunctionCppCaller(bSet), 1);

			lua_rawseti(L, -2, LUAPP11_PROPERTY_SET);

			luaL_newmetatable(L, LUAPP11_PROPERTY_METATABLE);
			lua_setmetatable(L, -2);

			lua_rawset(L, -3);

		}

		template <typename Getter>
		void RegistrationTree::RegisterProperty(lua_State *L, std::string name, Getter get)
		{

			lua_pushstring(L, name.c_str());

			lua_newtable(L);

			auto bGet = detail::MakeFunction(get);

			new (L) decltype(bGet) (bGet);
			lua_pushcclosure(L, detail::MakeFunctionCppCaller(bGet), 1);

			lua_rawseti(L, -2, LUAPP11_PROPERTY_GET);

			luaL_newmetatable(L, LUAPP11_PROPERTY_METATABLE);
			lua_setmetatable(L, -2);

			lua_rawset(L, -3);

		}

		LUAPP11_INLINE int LuaNoop(lua_State *L) { return 0; }

		template <typename T>
		void RegistrationTree::RegisterEnum(lua_State *L, std::string name, std::unordered_map<std::string, lua_Integer> *e)
		{

			lua_pushstring(L, name.c_str());

			lua_newtable(L);

			lua_newtable(L);

			for (auto &p : *e)
			{
				lua_pushstring(L, p.first.c_str());
				lua_pushinteger(L, p.second);
				lua_rawset(L, -3);
			}

			lua_pushstring(L, "__newindex");
			lua_pushcfunction(L, &LuaNoop);
			lua_rawset(L, -3);

			lua_pushstring(L, "__index");
			lua_pushvalue(L, -2);
			lua_rawset(L, -3);

			lua_setmetatable(L, -2);

			lua_rawset(L, -3);

		}

	}
}