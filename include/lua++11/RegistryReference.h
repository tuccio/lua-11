#pragma once

#include <memory>
#include <lua.hpp>

#include "Defines.h"

namespace luapp11
{

	class RegistryReference
	{

	public:

		RegistryReference(void);
		RegistryReference(RegistryReference && ref);

		~RegistryReference(void);

		bool Create(lua_State *L, int index);
		void Destroy(void);
		void Clear(void);

		LUAPP11_INLINE lua_State* GetState(void) const;
		LUAPP11_INLINE int        GetReference(void) const;

		LUAPP11_INLINE operator bool() const;

	private:

		lua_State *m_state;
		int        m_ref;

	};

	lua_State* RegistryReference::GetState(void) const
	{
		return m_state;
	}
	
	int RegistryReference::GetReference(void) const
	{
		return m_ref;
	}

	RegistryReference::operator bool() const
	{
		return m_state && m_ref != LUA_NOREF;
	}

	typedef std::shared_ptr<RegistryReference> SharedRegistryReference;

	SharedRegistryReference MakeSharedReference(RegistryReference && ref);

	LUAPP11_INLINE void Push(const RegistryReference &ref)
	{
		lua_rawgeti(ref.GetState(), LUA_REGISTRYINDEX, ref.GetReference());
	}

}