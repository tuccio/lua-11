#pragma once

#include <lua.hpp>

#include "Helpers.h"

namespace luapp11
{

	namespace detail
	{

		template <int N, typename Type, typename ... Types>
		struct SignatureTupleGenerator :
			SignatureTupleGenerator < N - 1, Types ..., std::type_index >
		{

		};

		template <typename ... Types>
		struct SignatureTupleGenerator < 0, Types ... >
		{
			typedef std::tuple<Types ...> type;
		};

		template <typename ... Types>
		struct Signature
		{

			Signature(void) :
				tuple(std::make_tuple(std::type_index(typeid(Types)) ...))
			{
			}

			typename SignatureTupleGenerator<sizeof ... (Types), Types ...>::type tuple;

		};

		template <typename R, typename ... Args>
		Signature<R, Args ...> DeduceSignature(R(*f) (Args ...))
		{
			return Signature<R, Args ...>();
		}

	}

	template <typename R, typename ... Args>
	int SignatureCheck(lua_State *L)
	{

		auto sequence = detail::IntegerSequenceGenerator<sizeof ... (Args)>::type();

		return detail::LuaToCppTupleConverter<Args ...>::IsConvertible(L, sequence) ? 1 : 0;

	}

}