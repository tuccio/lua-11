#pragma once

#include "Allocation.h"
#include "ClassRegistry.h"
#include "Defines.h"
#include "Functions.h"
#include "RegistrationForwardDeclarations.h"
#include "Overload.h"
#include "Object.h"

#include <lua.hpp>

#include <initializer_list>
#include <cstdarg>


namespace luapp11
{

	enum class Library
	{
		base,
		coroutine,
		table,
		io,
		os,
		string,
		math,
		debug,
		package
	};

	class State
	{

	public:

		State(void);
		~State(void);

		bool Create(void);
		void Destroy(void);

		bool RunCode(const char *code);
		bool RunFile(const char *file);

		LUAPP11_INLINE lua_State * GetState(void);

		void OpenLibrary(Library library);

		void OpenLibraries(void);
		void OpenLibraries(std::initializer_list<Library> libraries);

		Object GetGlobalTable(void);

		LUAPP11_INLINE operator bool ();

		void operator [] (detail::RegistrationTree & rt);
		void operator [] (Registrable & r);

		template <typename R, typename ... Args>
		typename std::enable_if<std::is_void<R>::value>::type Call(const char *f, Args && ... args)
		{

			lua_pushglobaltable(m_lua);
			
			lua_getfield(m_lua, -1, f);
			lua_replace(m_lua, -2);

			lua_call(m_lua, sizeof ... (Args), 0);

		}

		LUAPP11_INLINE operator lua_State * () { return m_lua; }

	private:

		void RegisterPrimitiveTypes(void);

		lua_State *m_lua;

	};

}


#include "State.inl"