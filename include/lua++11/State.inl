#include "Debug.h"
#include "Overload.h"
#include "Bind.h"

namespace luapp11
{

	
	/* Getters */

	lua_State* State::GetState(void)
	{
		return m_lua;
	}

	State::operator bool()
	{
		return m_lua != nullptr;
	}

}