#pragma once

#include <memory>
#include <type_traits>
#include <functional>

namespace luapp11
{

	namespace detail
	{

		/* Integer sequence */

		template <int ... N>
		struct IntegerSequence
		{

		};

		template <int Max, int ... Sequence>
		struct IntegerSequenceGenerator :
			IntegerSequenceGenerator<Max - 1, Max - 1, Sequence ... >
		{

		};

		template <int ... Sequence>
		struct IntegerSequenceGenerator<0, Sequence ...>
		{
			typedef IntegerSequence<Sequence ...> type;
		};

		

		template <typename T>
		struct RemoveQualifiers :
			public std::remove_const<std::remove_pointer_t<std::remove_reference_t<T>>>
		{

		};

		template <typename ... Variadic>
		struct IsEmptyTemplate;

		template <typename ... Variadic>
		struct IsEmptyTemplate<Variadic ...> :
			public std::false_type { };

		template <>
		struct IsEmptyTemplate<> :
			public std::true_type { };

		template <typename T>
		struct IsSharedPtr :
			public std::false_type { };

		template <typename T>
		struct IsSharedPtr<std::shared_ptr<T>> :
			public std::true_type { };

		template <typename T>
		struct IsFunction :
			public std::integral_constant<bool,
				std::is_function<typename std::remove_pointer<T>::type>::value ||
				std::is_member_function_pointer<T>::value> { };

		template <typename F>
		struct IsFunction<std::function<F>> :
			public std::true_type { };

	}

}