#pragma once

#include <typeindex>
#include <unordered_map>

#include <list>

namespace luapp11
{

	namespace detail
	{

		struct TypesGraphNode
		{
			std::vector<TypesGraphNode*> adjacents;
		};

	}

	class TypesManager
	{

	public:

		TypesManager(void);
		~TypesManager(void);

	private:

		std::unordered_map<std::type_index, detail::TypesGraphNode> m_map;

	};

}

