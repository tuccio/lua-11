#pragma once

#include <type_traits>
#include <memory>

#include "ClassRegistry.h"
#include "Types.h"
#include "Proxy.h"

#define LUAPP11_WRAPPED_TYPE_REGULAR    (0x1)
#define LUAPP11_WRAPPED_TYPE_PROXY      (0x2)
#define LUAPP11_WRAPPED_TYPE_SHARED_PTR (0x4)
#define LUAPP11_WRAPPED_TYPE_CONST      (0x8)

#define LUAPP11_WRAPPED_TYPE_REGULAR_NON_CONST      (LUAPP11_WRAPPED_TYPE_REGULAR)
#define LUAPP11_WRAPPED_TYPE_REGULAR_CONST          (LUAPP11_WRAPPED_TYPE_REGULAR | LUAPP11_WRAPPED_TYPE_CONST)
#define LUAPP11_WRAPPED_TYPE_PROXY_NON_CONST        (LUAPP11_WRAPPED_TYPE_PROXY)
#define LUAPP11_WRAPPED_TYPE_PROXY_CONST            (LUAPP11_WRAPPED_TYPE_PROXY | LUAPP11_WRAPPED_TYPE_CONST)
#define LUAPP11_WRAPPED_TYPE_SHARED_PTR_NON_CONST   (LUAPP11_WRAPPED_TYPE_SHARED_PTR)
#define LUAPP11_WRAPPED_TYPE_SHARED_PTR_CONST       (LUAPP11_WRAPPED_TYPE_SHARED_PTR | LUAPP11_WRAPPED_TYPE_CONST)



namespace luapp11
{

	namespace detail
	{

		template <typename T>
		void NoopDestructor(T *) { };
		
		/* WrappedTypeInfo */

		template <typename ValueType>
		struct WrappedTypeInfo
		{
			
			typedef typename std::remove_reference<ValueType>::type held_type;
			typedef held_type type;
			typedef typename std::remove_const<held_type>::type stripped_type;
			typedef RTTI<stripped_type> rtti_type;

			LUAPP11_INLINE static int GetType(void) { return LUAPP11_WRAPPED_TYPE_REGULAR | (std::is_const<held_type>::value ? LUAPP11_WRAPPED_TYPE_CONST : 0); };

		};

		template <typename ValueType>
		struct WrappedTypeInfo<Proxy<ValueType>>
		{

			typedef typename std::remove_reference<ValueType>::type held_type;
			typedef Proxy<held_type> type;
			typedef typename std::remove_const<held_type>::type stripped_type;
			typedef RTTI<stripped_type> rtti_type;

			LUAPP11_INLINE static int GetType(void) { return LUAPP11_WRAPPED_TYPE_PROXY | (std::is_const<held_type>::value ? LUAPP11_WRAPPED_TYPE_CONST : 0); };

		};

		template <typename ValueType>
		struct WrappedTypeInfo<std::shared_ptr<ValueType>>
		{

			typedef typename std::remove_reference<ValueType>::type held_type;
			typedef std::shared_ptr<held_type> type;
			typedef typename std::remove_const<held_type>::type stripped_type;
			typedef RTTI<stripped_type> rtti_type;

			LUAPP11_INLINE static int GetType(void) { return LUAPP11_WRAPPED_TYPE_SHARED_PTR | (std::is_const<held_type>::value ? LUAPP11_WRAPPED_TYPE_CONST : 0); };

		};

		/* Wrappers */

		class BaseWrapper
		{

		public:

			BaseWrapper(void) :
				m_type(0x0),
				m_cid(LUAPP11_CLASSID_INVALID) { }

			virtual ~BaseWrapper(void)
			{

			}

			LUAPP11_INLINE ClassID GetClassID(void) const
			{
				return m_cid;
			}

			LUAPP11_INLINE bool IsSharedPtr(void) const
			{
				return (m_type & LUAPP11_WRAPPED_TYPE_SHARED_PTR) != 0;
			}

			LUAPP11_INLINE bool IsProxy(void) const
			{
				return (m_type & LUAPP11_WRAPPED_TYPE_PROXY) != 0;
			}

			LUAPP11_INLINE bool IsRegular(void) const
			{
				return (m_type & LUAPP11_WRAPPED_TYPE_REGULAR) != 0;
			}

			LUAPP11_INLINE bool IsConst(void) const
			{
				return (m_type & LUAPP11_WRAPPED_TYPE_CONST) != 0;
			}

			LUAPP11_INLINE int GetType(void) const
			{
				return m_type;
			}

			virtual BaseWrapper* GetShallowCopy(void) = 0;

		protected:

			ClassID       m_cid;
			unsigned char m_type;

		};

		template <typename ValueType, typename Enable = void>
		class ValueWrapper;

		/* Regular ValueWrapper for abstract classes */

		template <typename ValueType>
		class ValueWrapper<ValueType, typename std::enable_if<std::is_same<ValueType, typename std::remove_reference<ValueType>::type>::value && !std::is_abstract<ValueType>::value && !IsSharedPtr<ValueType>::value && !IsProxy<ValueType>::value>::type> :
			public BaseWrapper
		{

		public:

			ValueWrapper(void) :
				m_value(nullptr)
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			ValueWrapper(const ValueType &v) :
				m_value(new ValueType(v))
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}
			ValueWrapper(ValueType && v) :
				m_value(new ValueType(std::forward<ValueType>(v)))
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			ValueWrapper(ValueType **v) :
				m_value(*v)
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			~ValueWrapper(void)
			{
				if (m_value) delete m_value;
			}

			LUAPP11_INLINE ValueType* Get(void)
			{
				return m_value;
			}

			ValueWrapper<Proxy<ValueType>> * GetShallowCopy(void)
			{
				return new ValueWrapper<Proxy<ValueType>>(m_value);
			}

		private:

			ValueType *m_value;

		};

		/* Regular ValueWrapper for abstract classes */

		template <typename ValueType>
		class ValueWrapper<ValueType, typename std::enable_if<std::is_same<ValueType, typename std::remove_reference<ValueType>::type>::value && std::is_abstract<ValueType>::value && !IsSharedPtr<ValueType>::value && !IsProxy<ValueType>::value>::type> :
			public BaseWrapper
		{

		public:

			ValueWrapper(void) :
				m_value(nullptr)
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			ValueWrapper(const ValueType &v) :
				m_value(nullptr)
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			ValueWrapper(ValueType && v) :
				m_value(nullptr)
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			~ValueWrapper(void)
			{
			}

			LUAPP11_INLINE ValueType* Get(void)
			{
				return m_value;
			}

			ValueWrapper<Proxy<ValueType>> * GetShallowCopy(void)
			{
				return new ValueWrapper<Proxy<ValueType>>(nullptr);
			}

		private:

			ValueType *m_value;

		};

		/* shared_ptr ValueWrapper for non abstract classes */

		template <typename ValueType>
		class ValueWrapper<ValueType, typename std::enable_if<std::is_same<ValueType, typename std::remove_reference<ValueType>::type>::value && !std::is_abstract<ValueType>::value && IsSharedPtr<ValueType>::value>::type> :
			public BaseWrapper
		{

		public:

			ValueWrapper(void) :
				m_value(nullptr)
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			ValueWrapper(const ValueType &v) :
				m_value(new ValueType(v))
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}
			ValueWrapper(ValueType && v) :
				m_value(new ValueType(std::forward<ValueType>(v)))
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			ValueWrapper(ValueType **v) :
				m_value(*v)
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			~ValueWrapper(void)
			{
				if (m_value) delete m_value;
			}

			LUAPP11_INLINE ValueType* Get(void)
			{
				return m_value;
			}

			ValueWrapper<ValueType> * GetShallowCopy(void)
			{
				return new ValueWrapper<ValueType>(*m_value);
			}

		private:

			ValueType *m_value;

		};

		/* Proxy ValueWrapper */

		template <typename ValueType>
		class ValueWrapper<ValueType, typename std::enable_if<std::is_same<ValueType, typename std::remove_reference<ValueType>::type>::value && IsProxy<ValueType>::value>::type> :
			public BaseWrapper
		{

		public:

			ValueWrapper(void) :
				m_value(nullptr)
			{
				m_type = WrappedTypeInfo<ValueType>::GetType();
				m_cid  = WrappedTypeInfo<ValueType>::rtti_type::id;
			}

			ValueWrapper(typename WrappedTypeInfo<ValueType>::held_type * v) :
				m_value(nullptr)
			{
				m_type  = WrappedTypeInfo<ValueType>::GetType();
				m_cid   = WrappedTypeInfo<ValueType>::rtti_type::id;
				m_value = v;
			}

			ValueWrapper(const ValueType &v) :
				m_value(nullptr)
			{
				m_type  = WrappedTypeInfo<ValueType>::GetType();
				m_cid   = WrappedTypeInfo<ValueType>::rtti_type::id;
				m_value = v.value;
			}

			LUAPP11_INLINE typename WrappedTypeInfo<ValueType>::held_type * Get(void)
			{
				return m_value;
			}

			ValueWrapper<ValueType> * GetShallowCopy(void)
			{
				return new ValueWrapper<ValueType>(m_value);
			}

		private:

			typename WrappedTypeInfo<ValueType>::held_type * m_value;

		};

		class LuaObjectWrapper
		{

		public:

			LuaObjectWrapper(void)
			{
				m_wrapper = nullptr;
			}

			template <typename ValueType>
			LuaObjectWrapper(const ValueType &v)
			{
				m_wrapper = new ValueWrapper<ValueType>(v);
			}

			template <typename ValueType>
			LuaObjectWrapper(ValueType && v, typename std::enable_if<std::is_same<ValueType, typename std::remove_reference<ValueType>::type>::value>::type * = nullptr)
			{
				m_wrapper = new ValueWrapper<ValueType>(std::forward<ValueType>(v));
			}

			template <typename ValueType>
			LuaObjectWrapper(ValueType *v, typename std::enable_if<std::is_same<ValueType, typename std::remove_reference<ValueType>::type>::value>::type * = nullptr)
			{
				m_wrapper = new ValueWrapper<Proxy<ValueType>>(v);
			}

			template <typename ValueType>
			LuaObjectWrapper(ValueType **v, typename std::enable_if<std::is_same<ValueType, typename std::remove_reference<ValueType>::type>::value>::type * = nullptr)
			{
				m_wrapper = new ValueWrapper<ValueType>(v);
			}

			LuaObjectWrapper(LuaObjectWrapper && o)
			{
				m_wrapper   = o.m_wrapper;
				o.m_wrapper = nullptr;
			}

			~LuaObjectWrapper(void)
			{
				Clear();
			}

			LuaObjectWrapper& operator= (LuaObjectWrapper && o)
			{

				Clear();

				m_wrapper   = o.m_wrapper;
				o.m_wrapper = nullptr;
				return *this;

			}

			LUAPP11_INLINE bool IsEmpty(void)
			{
				return m_wrapper == nullptr;
			}

			LUAPP11_INLINE ClassID GetClassID(void)
			{
				return m_wrapper->GetClassID();
			}

			LUAPP11_INLINE bool IsConst(void)
			{
				return m_wrapper->IsConst();
			}

			void Clear(void)
			{

				if (m_wrapper)
				{
					delete m_wrapper;
				}

				m_wrapper = nullptr;

			}

			template <typename T>
			LuaObjectWrapper Upcast(lua_State *L, ClassID cid)
			{

				if (m_wrapper->IsSharedPtr())
				{

					/* shared_ptr */

					LuaObjectWrapper o(m_wrapper->GetShallowCopy());
					ClassID currentCid = GetClassID();

					ClassRegistry     *registry  = ClassRegistry::GetInstance(L);
					ClassRegistryInfo *classInfo = registry->GetClass(currentCid);

					std::deque<ClassID> path;

					if (registry->GetPathToBase(cid, currentCid, &path))
					{

						for (ClassID baseCid : path)
						{

							ClassRegistryInfo *currentClass = registry->GetClass(currentCid);
							UpcastFunction upcast = currentClass->sharedptrUpcastMap[baseCid];

							o = std::move((*upcast)(o));
							currentCid = baseCid;

						}

					}

					return std::move(o);

				}
				else
				{

					/* Regular/proxy */

					LuaObjectWrapper currentObject(m_wrapper->GetShallowCopy());

					ClassID currentCid = GetClassID();

					ClassRegistry     *registry  = ClassRegistry::GetInstance(L);
					ClassRegistryInfo *classInfo = registry->GetClass(currentCid);

					std::deque<ClassID> path;

					if (registry->GetPathToBase(cid, currentCid, &path))
					{

						for (ClassID baseCid : path)
						{

							ClassRegistryInfo *currentClass = registry->GetClass(currentCid);
							UpcastFunction upcast = currentClass->upcastMap[baseCid];

							currentObject = std::move((*upcast)(currentObject));

							currentCid = baseCid;

						}

					}

					return std::move(currentObject);

				}

			}

			template <typename ValueType>
			ValueType* GetPointer(void)
			{

				typedef std::remove_const<ValueType>::type stripped_type;
				assert(m_wrapper->GetClassID() == RTTI<stripped_type>::id);
				
				ValueType * p = nullptr;

				switch (m_wrapper->GetType())
				{
				case LUAPP11_WRAPPED_TYPE_REGULAR_CONST:
					p = const_cast<ValueType*>(static_cast<ValueWrapper<const stripped_type>*>(m_wrapper)->Get()); break;
				case LUAPP11_WRAPPED_TYPE_REGULAR_NON_CONST:
					p = const_cast<ValueType*>(static_cast<ValueWrapper<stripped_type>*>(m_wrapper)->Get()); break;
				case LUAPP11_WRAPPED_TYPE_SHARED_PTR_CONST:
					p = const_cast<ValueType*>(static_cast<ValueWrapper<std::shared_ptr<const stripped_type>>*>(m_wrapper)->Get()->get()); break;
				case LUAPP11_WRAPPED_TYPE_SHARED_PTR_NON_CONST:
					p = const_cast<ValueType*>(static_cast<ValueWrapper<std::shared_ptr<stripped_type>>*>(m_wrapper)->Get()->get()); break;
				case LUAPP11_WRAPPED_TYPE_PROXY_CONST:
					p = const_cast<ValueType*>(static_cast<ValueWrapper<Proxy<const stripped_type>>*>(m_wrapper)->Get()); break;
				case LUAPP11_WRAPPED_TYPE_PROXY_NON_CONST:
					p = const_cast<ValueType*>(static_cast<ValueWrapper<Proxy<stripped_type>>*>(m_wrapper)->Get()); break;
				}

				return p;

			}

			template <typename ValueType>
			std::shared_ptr<ValueType> GetSharedPtr(void)
			{

				typedef std::remove_const<ValueType>::type stripped_type;
				assert(m_wrapper->GetClassID() == RTTI<stripped_type>::id);

				std::shared_ptr<ValueType> p;

				switch (m_wrapper->GetType())
				{
				case LUAPP11_WRAPPED_TYPE_REGULAR_CONST:
					p = std::shared_ptr<ValueType>(const_cast<ValueType*>(static_cast<ValueWrapper<const stripped_type>*>(m_wrapper)->Get()), &NoopDestructor<ValueType>); break;
				case LUAPP11_WRAPPED_TYPE_REGULAR_NON_CONST:
					p = std::shared_ptr<ValueType>(const_cast<ValueType*>(static_cast<ValueWrapper<stripped_type>*>(m_wrapper)->Get()), &NoopDestructor<ValueType>); break;
				case LUAPP11_WRAPPED_TYPE_SHARED_PTR_CONST:
					p = std::const_pointer_cast<ValueType>(*static_cast<ValueWrapper<std::shared_ptr<const stripped_type>>*>(m_wrapper)->Get()); break;
				case LUAPP11_WRAPPED_TYPE_SHARED_PTR_NON_CONST:
					p = std::const_pointer_cast<ValueType>(*static_cast<ValueWrapper<std::shared_ptr<stripped_type>>*>(m_wrapper)->Get()); break;
				case LUAPP11_WRAPPED_TYPE_PROXY_CONST:
					p = std::shared_ptr<ValueType>(const_cast<ValueType*>(static_cast<ValueWrapper<Proxy<const stripped_type>>*>(m_wrapper)->Get()), &NoopDestructor<ValueType>); break;
				case LUAPP11_WRAPPED_TYPE_PROXY_NON_CONST:
					p = std::shared_ptr<ValueType>(const_cast<ValueType*>(static_cast<ValueWrapper<Proxy<stripped_type>>*>(m_wrapper)->Get()), &NoopDestructor<ValueType>); break;
				}

				return std::move(p);

			}

		private:

			BaseWrapper *m_wrapper;

			LuaObjectWrapper(BaseWrapper *wrapper) : m_wrapper(wrapper) { }

		};

		typedef LuaObjectWrapper(*WrapperCastFunction) (LuaObjectWrapper&);

		template <typename From, typename To>
		typename std::enable_if<!IsSharedPtr<From>::value && !IsSharedPtr<To>::value, LuaObjectWrapper>::type Cast(LuaObjectWrapper &object)
		{
			From *f = object.GetPointer<From>();
			return LuaObjectWrapper(static_cast<To*>(f));
		}

		template <typename From, typename To>
		typename std::enable_if<IsSharedPtr<From>::value && IsSharedPtr<To>::value, LuaObjectWrapper>::type Cast(LuaObjectWrapper &object)
		{
			std::shared_ptr<From::element_type> f = object.GetSharedPtr<From::element_type>();
			std::shared_ptr<To::element_type>   t = std::static_pointer_cast<To::element_type>(f);
			return LuaObjectWrapper(t);
		}

	}

}