shapes = { }

math.randomseed(os.time())

for i = 1, 200 do

	
	local r = math.random()
	
	local randomvec2 = function() return vec2(math.random(), math.random()) end
	local scale = 4
	
	if (r > 0.5) then
		shapes[i] = Circle(scale * randomvec2() - (scale * 0.5), math.random() * 0.5)
	else
	
		local width  = math.random()
		local height = math.random()
		
		local p1 = scale * randomvec2() - (scale * 0.5)
		shapes[i] = Rectangle(p1, vec2(width, height) + p1)
		
	end
	
	shapes[i].color = 0.35 + vec3( math.random(), math.random(), math.random() ) * 0.55
	
end

function Render()

	for i, shape in ipairs(shapes) do
		shape:Draw()
	end

end

function Update()
	
	for i, shape in ipairs(shapes) do
	
		local dv
		
		if (shape.name == "Rectangle") then
			dv = vec2(math.random() - 0.5, math.random() * 0.5 - 0.25) * 0.05
		elseif (shape.name == "Circle") then
			dv = vec2(math.random() * 0.5 - 0.25, math.random() - 0.5) * 0.05
		end
		
		shape.position = shape.position + dv
		
	end

end