#define _USE_MATH_DEFINES

#include <lua++11/lua++11.h>

#include <cmath>
#include <iostream>
#include <sstream>
#include <string>

#include <gl/glut.h>
#include <gl/GLU.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define CIRCLE_PRECISION_LEVEL 100
#define UPDATE_TIME 10

static luapp11::State g_state;
static glm::mat4      g_projection;

struct Drawable
{

	Drawable(void) : color(1, 1, 1) { }

	void Draw(void) { }

	glm::vec3 color;

};

struct Movable
{

	Movable(void) :
		position(0, 0)
	{
	}
	
	glm::vec2 position;

};

struct Shape :
	public Movable
{

public:

	Shape(const char *name) : name(name) { }

	virtual float GetArea(void) const = 0;

	std::string name;

};

struct Rectangle :
	public Shape, public Drawable
{
	
	Rectangle(const glm::vec2 &p1, const glm::vec2 &p2) :
		Shape("Rectangle"),
		p1(p1),
		p2(p2)
	{

	}

	float GetArea(void) const
	{
		float r = (p1.x - p2.x) * (p1.y - p2.y);
		return (r > 0 ? r : -r);
	}

	void Draw(void)
	{
		glColor3fv(&color.x);
		auto p1t = p1 + position;
		auto p2t = p2 + position;
		glRectfv(&p1t.x, &p2t.x);
	}

	glm::vec2 p1;
	glm::vec2 p2;

};

struct Circle :
	public Shape, public Drawable
{

	Circle(const glm::vec2 &center, float radius) :
		Shape("Circle"),
		center(center),
		radius(radius)
	{

	}

	float GetArea(void) const
	{
		return M_PI * radius * radius;
	}

	void Draw(void)
	{

		glColor3fv(&color.x);
		glBegin(GL_TRIANGLE_STRIP);

		float theta = 0.0f;
		float dTheta = (2.0f * M_PI) / CIRCLE_PRECISION_LEVEL;

		for (int i = 0; i <= CIRCLE_PRECISION_LEVEL; i++)
		{

			glm::vec2 x = glm::vec2(cos(theta), sin(theta)) * radius + center + position;
			glm::vec2 c = center + position;

			glVertex2fv(&x.x);
			glVertex2fv(&c.x);

			theta += dTheta;

		}

		glEnd();

	}

	float     radius;
	glm::vec2 center;

};

void Render(void)
{

	glClear(GL_COLOR_BUFFER_BIT);

	g_state.Call<void>("Render");

	glutSwapBuffers();

}

void Update(int param)
{
	
	g_state.Call<void>("Update");

	glutPostRedisplay();

	glutTimerFunc(UPDATE_TIME, &Update, param);

}

void Reshape(int width, int height)
{

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0f, (float) width / height, 0.0f, 100.0f);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	glTranslatef(0, 0, -2);

}

int main(int argc, char *argv[])
{

	using namespace luapp11;

	g_state.Create();
	g_state.OpenLibraries();

	g_state
	[

		Function("Halt", std::function<void(void)>([] () { __debugbreak(); })),

		Class<glm::vec3>("vec3").

			Constructor<>().
			Constructor<float, float, float>().

			Operator(Operand<const glm::vec3&>() + Operand<const glm::vec3&>()).
			Operator(Operand<const glm::vec3&>() - Operand<const glm::vec3&>()).

			Operator(Operand<float>() * Operand<const glm::vec3&>()).
			Operator(Operand<const glm::vec3&>() * Operand<float>()).

			Operator(Operand<float>() + Operand<const glm::vec3&>()).
			Operator(Operand<const glm::vec3&>() + Operand<float>()).

			Operator(Operand<float>() - Operand<const glm::vec3&>()).
			Operator(Operand<const glm::vec3&>() - Operand<float>()).

			Property("x", &glm::vec3::x).
			Property("y", &glm::vec3::y).
			Property("z", &glm::vec3::z).
			
			Function("__tostring", std::function<std::string(const glm::vec3&)>(
				[](const glm::vec3 &v) -> std::string
				{
					std::stringstream ss;
					ss << "[" << v.x << ", " << v.y << ", " << v.z << "]";
					return ss.str();
				})
			),

		Class<glm::vec2>("vec2").

			Constructor<>().
			Constructor<float, float>().

			Operator(Operand<const glm::vec2&>() + Operand<const glm::vec2&>()).
			Operator(Operand<const glm::vec2&>() - Operand<const glm::vec2&>()).

			Operator(Operand<float>() * Operand<const glm::vec2&>()).
			Operator(Operand<const glm::vec2&>() * Operand<float>()).

			Operator(Operand<float>() + Operand<const glm::vec2&>()).
			Operator(Operand<const glm::vec2&>() + Operand<float>()).

			Operator(Operand<float>() - Operand<const glm::vec2&>()).
			Operator(Operand<const glm::vec2&>() - Operand<float>()).

			Property("x", &glm::vec2::x).
			Property("y", &glm::vec2::y).

			Function("__tostring", std::function<std::string(const glm::vec2&)>(
				[](const glm::vec2 &v) -> std::string
				{
					std::stringstream ss;
					ss << "[" << v.x << ", " << v.y << "]";
					return ss.str();
				})
			),

		Class<Movable>("Movable").

			Property("position", &Movable::position),

		Class<struct Drawable>("Drawable").
		
			Function("Draw", &Drawable::Draw).

			Property("color", &Drawable::color),

		Class<struct Shape, struct Movable>("Shape").

			Function("GetArea", &Shape::GetArea).

			Property("name", &Shape::name),


		Class<struct Circle, struct Shape, struct Drawable>("Circle").

			Constructor<const glm::vec2 &, float>().

			Function("Draw", &Circle::Draw).

			Property("center", &Circle::center).
			Property("radius", &Circle::radius),

		Class<struct Rectangle, struct Shape, struct Drawable>("Rectangle").

			Constructor<const glm::vec2 &, const glm::vec2 &>().

			Function("Draw", &Rectangle::Draw).

			Property("p1", &Rectangle::p1).
			Property("p2", &Rectangle::p2)

	];

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowPosition(32, 32);

	glutInitWindowSize(960, 600);
	
	glutCreateWindow("Inheritance test");

	g_state.RunFile("InheritanceTest.lua");

	//glutIdleFunc(&Update);
	glutTimerFunc(UPDATE_TIME, &Update, 0);
	glutDisplayFunc(&Render);
	glutReshapeFunc(&Reshape);

	glDisable(GL_DEPTH_TEST);

	glutMainLoop();

	g_state.Destroy();

	return 0;

}