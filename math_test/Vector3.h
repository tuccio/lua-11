#pragma once

#include <cmath>
#include <string>
#include <ostream>
#include <sstream>

#include <malloc.h>

#define __ALIGNED 1

#if __ALIGNED
#define __ALIGN(N) __declspec(align(N))
#else
#define __ALIGN(N)
#endif

template <typename T>
struct __ALIGN(16) Vector3
{

	Vector3(void) { }

	Vector3(const T &x, const T &y, const T &z) :
		x(x), y(y), z(z) { }

	T Length(void) const
	{
		return sqrt(x * x + y * y + z * z);
	}

	Vector3 Normalize(void) const
	{
		return *this / this->Length();
	}

	void* operator new (size_t size)
	{

		void *p = _aligned_malloc(size, 16);

		if (!p)
		{
			throw std::bad_alloc();
		}

		return p;

	}

	void* operator new (size_t size, const std::nothrow_t &ntv)
	{
		return _aligned_malloc(size, 16);
	}

	void* operator new(size_t size, void *p)
	{
		return p;
	}

	void operator delete(void *p)
	{
		_aligned_free(p);
	}

	void operator delete(void *p, void* p2) { }

	T x, y, z;

};

template <typename T>
Vector3<T> operator+ (const Vector3<T>& v1, const Vector3<T>& v2)
{
	return Vector3<T>(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

template <typename T>
Vector3<T> operator- (const Vector3<T>& v1, const Vector3<T>& v2)
{
	return Vector3<T>(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

template <typename T>
Vector3<T> operator+ (const Vector3<T>& v1, T s)
{
	return Vector3<T>(v1.x + s, v1.y + s, v1.z + s);
}

template <typename T>
Vector3<T> operator- (const Vector3<T>& v1, T s)
{
	return Vector3<T>(v1.x - s, v1.y - s, v1.z - s);
}

template <typename T>
Vector3<T> operator+ (T s, const Vector3<T>& v1)
{
	return Vector3<T>(s + v1.x, s + v1.y, s + v1.z);
}


template <typename T>
Vector3<T> operator- (T s, const Vector3<T>& v1)
{
	return Vector3<T>(s - v1.x, s - v1.y, s - v1.z);
}

template <typename T>
Vector3<T> operator* (T s, const Vector3<T> &v)
{
	return Vector3<T>(s * v.x, s * v.y, s * v.z);
}

template <typename T>
Vector3<T> operator* (const Vector3<T> &v, T s)
{
	return Vector3<T>(s * v.x, s * v.y, s * v.z);
}

template <typename T>
Vector3<T> operator/ (const Vector3<T> &v, T s)
{
	return v * T(1 / s);
}

template <typename T>
Vector3<T> operator/ (T s, const Vector3<T> &v)
{
	return Vector3<T>(s / v.x, s / v.y, s / v.z);
}

template <typename T>
std::ostream& operator<< (std::ostream &os, const Vector3<T> &v)
{
	return (os << "[" << v.x << ", " << v.y << ", " << v.z << "]");
}

typedef Vector3<float> Vector3f;