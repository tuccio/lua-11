#include "../lua++11/lua++11.h"
#include "../lua++11/SharedPointers.h"

#include "Vector3.h"

#include <functional>
#include <type_traits>
#include <iostream>

template <typename T>
void PrintVector3(const Vector3<T> *v)
{
	std::cout << *v << std::endl;
}

template <typename T>
void RegisterVector3(luapp11::State state, const char *name)
{

	using namespace luapp11;

	state
	[

		Class<Vector3<T>>(name).

			Constructor<>().
			//Constructor<const T&, const T&, const T&>().
			Constructor<T, T, T>().

			Function("Length", &Vector3<T>::Length).
			Function("Normalize", &Vector3<T>::Normalize).

			Operator(Operand<const Vector3<T>&>() + Operand<const Vector3<T>&>()).
			Operator(Operand<const Vector3<T>&>() - Operand<const Vector3<T>&>()).

			Operator(Operand<const Vector3<T>&>() + Operand<float>()).
			Operator(Operand<const Vector3<T>&>() - Operand<float>()).

			Operator(Operand<float>() + Operand<const Vector3<T>&>()).
			Operator(Operand<float>() - Operand<const Vector3<T>&>()).

			Operator(Operand<float>() * Operand<const Vector3<T>&>()).
			Operator(Operand<const Vector3<T>&>() * Operand<T>()).

			Operator(Operand<const Vector3<T>&>() / Operand<T>()).
			Operator(Operand<T>() / Operand<const Vector3<T>&>()).
			
			Property("x", &Vector3<T>::x).
			Property("y", &Vector3<T>::y).
			Property("z", &Vector3<T>::z),

		Function("PrintVector3", &PrintVector3<T>)

	];

}

int main(int argc, char *argv[])
{

	using namespace luapp11;

	State state;
	
	state.Create();
	state.OpenLibraries();

	RegisterVector3<float>(state, "Vector3f");

	state.RunFile("test.lua");

	std::string line;

	while (!std::cin.eof())
	{

		std::getline(std::cin, line);

		if (!line.empty())
		{
			state.RunCode(line.c_str());
		}

	}

	state.Destroy();

	return 0;

}