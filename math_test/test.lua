local v1 = Vector3f(5, 3, 2)
local v2 = Vector3f(-1, 3, 4)
--local v2 = Vector3f(2, 4, 6)

--PrintVector3(v1 + v1)
--
--PrintVector3(1 + v1)
--PrintVector3(v1 + 1)

--PrintVector3(v1 - v1)
--PrintVector3(1 - v1)
local n = 4
PrintVector3(v1 + v2 - (n * 0.5))

--PrintVector3(2 * v1)
--PrintVector3(v1 * 2)

--[[
PrintVector3(v1)
PrintVector3(v2)

print(v1:Length())
print(v2:Length())

PrintVector3(v1 + v2)
PrintVector3(v1 - v2)

PrintVector3(2 * v1)
PrintVector3(v2 * 2)

function DotProduct(s, t)
	return s.x * t.x + s.y * t.y + s.z * t.z
end

print(DotProduct(v1, v2))

local v3 = Vector3f()

v3.x = 12
v3.y = 7
v3.z = 3

PrintVector3(v3)
PrintVector3(v3:Normalize())]]--