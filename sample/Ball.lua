local Ball = {}

function Ball:Init()

	self.data = DataComponent()
	
	self.data.radius       = 1
	self.data.color        = glm.vec3(1, 1, 0)
	
	self.data.velocity     = glm.vec3(0, 0, 0)
	self.data.acceleration = glm.vec3(0, 0, 0)
	
	self.keyboard = KeyboardComponent()
	
	self.render       = RenderComponent()
	self.render.color = glm.vec3(1, 1, 0)
	self.render.type  = RenderableObject.SPHERE
	
end

function Ball:Update(dt)

	local Euler = require 'Euler'
	
	local xv = Euler(self.transform.position, self.data.velocity, self.data.acceleration, dt)
	
	self.transform.position = xv.position
	self.data.velocity      = xv.velocity

end

function Ball:OnKeyPress(key)
	
	local da = 0.1
	
	if (key == KeyboardKey.LEFT) then
		self.data.acceleration.x = self.data.acceleration.x - da
	elseif (key == KeyboardKey.RIGHT) then
		self.data.acceleration.x = self.data.acceleration.x + da
	end
	
end

function Ball:OnKeyRelease(key)

	if (key == KeyboardKey.LEFT) then
		self.data.acceleration.x = 0
	elseif (key == KeyboardKey.RIGHT) then
		self.data.acceleration.x = 0
	end
	
end

return Ball