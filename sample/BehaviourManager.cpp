#include "BehaviourManager.h"


BehaviourManager::BehaviourManager(lua_State *L) :
	m_lua(L)
{
}


BehaviourManager::~BehaviourManager(void)
{
}

const luapp11::LuaBehaviour& BehaviourManager::Create(const char *name)
{

	auto it = m_behaviours.find(name);

	if (it != m_behaviours.end())
	{
		return it->second;
	}
	else
	{
		
		auto r = m_behaviours.insert(std::make_pair(name, luapp11::LuaBehaviour()));

		luapp11::LuaBehaviour& lb = r.first->second;
		lb.Create(m_lua, (std::string(name) + ".lua").c_str());
		return lb;

	}

}
