#pragma once

#include <unordered_map>
#include <lua++11/lua++11.h>

#include "Singleton.h"

class BehaviourManager :
	public Singleton<BehaviourManager>
{

public:

	BehaviourManager(lua_State *L);
	~BehaviourManager(void);

	const luapp11::LuaBehaviour& Create(const char *name);

private:

	std::unordered_map<std::string, luapp11::LuaBehaviour> m_behaviours;
	lua_State *m_lua;

};

