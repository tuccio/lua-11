#include "BindComponents.h"

#include "DynamicObject.h"
#include "ObjectsManager.h"
#include "Keyboard.h"

#include "Globals.h"

void BindComponents(luapp11::State &state)
{

	using namespace luapp11;

	state
	[

		Class<DynamicObject>("DynamicObject").

			Property("name", &DynamicObject::GetName).

			Property("transform",
				&DynamicObject::GetTransformComponent,
				std::function<void(DynamicObject *obj, const TransformComponent*)>(
					[](DynamicObject *obj, const TransformComponent *tc)
					{
						*obj->GetTransformComponent() = *tc;
					}
				)
			).

			Property("keyboard",
			&DynamicObject::GetKeyboardComponent,
				std::function<void(DynamicObject *obj, KeyboardComponent*)>(
					[](DynamicObject *obj, KeyboardComponent *kc)
					{

						Component *old = obj->SetComponent("keyboard", new KeyboardComponent);

						if (old) delete old;

					}
				)
			).

			Property("data",
				&DynamicObject::GetDataComponent,
				std::function<void(DynamicObject *obj, DataComponent*)>(
					[](DynamicObject *obj, DataComponent *dc)
					{

						Component *old = obj->SetComponent("data", new DataComponent(std::move(*dc)));

						if (old) delete old;

					}
				)
			).
			
			Property("render",
				&DynamicObject::GetRenderComponent,
				std::function<void(DynamicObject *obj, RenderComponent*)>(
					[](DynamicObject *obj, RenderComponent *rc)
					{

						Component *old = obj->SetComponent("render", new RenderComponent(std::move(*rc)));

						if (old) delete old;

					}
				)
			),

		Class<ObjectsManager>("ObjectsManager").
			Function("CreateObject",  &ObjectsManager::CreateObject).
			Function("CreateEntity",  &ObjectsManager::CreateEntity).
			Function("DestroyObject", &ObjectsManager::DestroyObject).
			Function("GetObject",     &ObjectsManager::GetObject),

		Class<Component>("Component"),

		Class<TransformComponent, Component>("TransformComponent").
			Property("position", &TransformComponent::position).
			Property("scaling",  &TransformComponent::scaling),

		Class<BehaviourComponent, Component>("BehaviourComponent").
			Constructor<const char*>(),

		Enum<RenderableObject>("RenderableObject")
			("NONE",   RenderableObject::NONE)
			("SPHERE", RenderableObject::SPHERE)
			("CUBE",   RenderableObject::CUBE),

		Class<RenderComponent, Component>("RenderComponent").
			Constructor<>().
			Property("color", &RenderComponent::color).
			Property("type",  &RenderComponent::type),

		Class<DataComponent, Component>("DataComponent").
			Constructor<>().
			Index(&DataComponentIndex).
			NewIndex(&DataComponentNewIndex),

		Class<Keyboard>("Keyboard").
			Function("AddKeyboardListener", &Keyboard::AddKeyboardListener),

		Class<KeyboardListener>("KeyboardListener"),

		Class<KeyboardComponent, KeyboardListener, Component>("KeyboardComponent").
			Constructor<>(),

		Enum<KeyboardKey>("KeyboardKey")
			("A",     KeyboardKey::A)
			("B",     KeyboardKey::B)
			("C",     KeyboardKey::C)
			("D",     KeyboardKey::D)
			("E",     KeyboardKey::E)
			("F",     KeyboardKey::F)
			("G",     KeyboardKey::G)
			("H",     KeyboardKey::H)
			("I",     KeyboardKey::I)
			("J",     KeyboardKey::J)
			("K",     KeyboardKey::K)
			("L",     KeyboardKey::L)
			("M",     KeyboardKey::M)
			("N",     KeyboardKey::N)
			("O",     KeyboardKey::O)
			("P",     KeyboardKey::P)
			("Q",     KeyboardKey::Q)
			("R",     KeyboardKey::R)
			("S",     KeyboardKey::S)
			("T",     KeyboardKey::T)
			("U",     KeyboardKey::U)
			("V",     KeyboardKey::V)
			("W",     KeyboardKey::W)
			("X",     KeyboardKey::X)
			("Y",     KeyboardKey::Y)
			("Z",     KeyboardKey::Z)
			("LEFT",  KeyboardKey::LEFT)
			("RIGHT", KeyboardKey::RIGHT)
			("UP",    KeyboardKey::UP)
			("DOWN",  KeyboardKey::DOWN)

	];

	Object g = g_state.GetGlobalTable();

	g["ObjectsManager"] = &g_objectsManager;

}