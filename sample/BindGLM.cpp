#include "BindGLM.h"

using namespace luapp11;

void BindGLM(State state)
{

	state
	[

		Namespace("glm")
		[

			Class<glm::vec4>("vec4").

				Constructor<>().
				Constructor<float, float, float, float>().

				Operator(Operand<const glm::vec4&>() + Operand<const glm::vec4&>()).
				Operator(Operand<const glm::vec4&>() - Operand<const glm::vec4&>()).

				Operator(Operand<float>() * Operand<const glm::vec4&>()).
				Operator(Operand<const glm::vec4&>() * Operand<float>()).

				Operator(Operand<float>() + Operand<const glm::vec4&>()).
				Operator(Operand<const glm::vec4&>() + Operand<float>()).

				Operator(Operand<float>() - Operand<const glm::vec4&>()).
				Operator(Operand<const glm::vec4&>() - Operand<float>()).

				Property("x", &glm::vec4::x).
				Property("y", &glm::vec4::y).
				Property("z", &glm::vec4::z).
				Property("w", &glm::vec4::w).

				Function("__tostring", std::function<std::string(const glm::vec4&)>(
					[](const glm::vec4 &v) -> std::string
					{
						std::stringstream ss;
						ss << "[" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << "]";
						return ss.str();
					})
				),

			Class<glm::vec3>("vec3").

				Constructor<>().
				Constructor<float, float, float>().

				Operator(Operand<const glm::vec3&>() + Operand<const glm::vec3&>()).
				Operator(Operand<const glm::vec3&>() - Operand<const glm::vec3&>()).

				Operator(Operand<float>() * Operand<const glm::vec3&>()).
				Operator(Operand<const glm::vec3&>() * Operand<float>()).

				Operator(Operand<float>() + Operand<const glm::vec3&>()).
				Operator(Operand<const glm::vec3&>() + Operand<float>()).

				Operator(Operand<float>() - Operand<const glm::vec3&>()).
				Operator(Operand<const glm::vec3&>() - Operand<float>()).

				Property("x", &glm::vec3::x).
				Property("y", &glm::vec3::y).
				Property("z", &glm::vec3::z).

				Function("__tostring", std::function<std::string(const glm::vec3&)>(
					[](const glm::vec3 &v) -> std::string
					{
						std::stringstream ss;
						ss << "[" << v.x << ", " << v.y << ", " << v.z << "]";
						return ss.str();
					})
				),

			Class<glm::vec2>("vec2").

				Constructor<>().
				Constructor<float, float>().

				Operator(Operand<const glm::vec2&>() + Operand<const glm::vec2&>()).
				Operator(Operand<const glm::vec2&>() - Operand<const glm::vec2&>()).

				Operator(Operand<float>() * Operand<const glm::vec2&>()).
				Operator(Operand<const glm::vec2&>() * Operand<float>()).

				Operator(Operand<float>() + Operand<const glm::vec2&>()).
				Operator(Operand<const glm::vec2&>() + Operand<float>()).

				Operator(Operand<float>() - Operand<const glm::vec2&>()).
				Operator(Operand<const glm::vec2&>() - Operand<float>()).

				Property("x", &glm::vec2::x).
				Property("y", &glm::vec2::y).

				Function("__tostring", std::function<std::string(const glm::vec2&)>(
					[](const glm::vec2 &v) -> std::string
					{
						std::stringstream ss;
						ss << "[" << v.x << ", " << v.y << "]";
						return ss.str();
					})
				)
		]
	];

}