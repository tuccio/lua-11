local Camera = {}

function Camera:Init()

	self.transform.position = glm.vec3(0, 0, 10)
	
	self.keyboard = KeyboardComponent()
	
	self.data = DataComponent()
	
	self.data.velocity     = glm.vec3(0, 0, 0)
	self.data.acceleration = glm.vec3(0, 0, 0)

end

function Camera:Update(dt)

	local Euler = require 'Euler'
	
	local xv = Euler(self.transform.position, self.data.velocity, self.data.acceleration, dt)
	
	self.transform.position = xv.position
	self.data.velocity      = xv.velocity
	
end

function Camera:OnKeyPress(key)

	local a = 1

	if (key == KeyboardKey.S) then
		self.data.acceleration.z = a
	elseif (key == KeyboardKey.W) then
		self.data.acceleration.z = -a
	elseif (key == KeyboardKey.A) then
		self.data.acceleration.x = -a
	elseif (key == KeyboardKey.D) then
		self.data.acceleration.x = a
	end
	
end

function Camera:OnKeyRelease(key)

	if (key == KeyboardKey.S or key == KeyboardKey.W) then
		self.data.acceleration.z = 0
	elseif (key == KeyboardKey.A or key == KeyboardKey.D) then
		self.data.acceleration.x = 0
	end

end

return Camera