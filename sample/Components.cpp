#include "Components.h"
#include "DynamicObject.h"

#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>

void RenderComponent::Render(void)
{
	switch (type)
	{

	case RenderableObject::SPHERE:

	{
		glColor3fv(glm::value_ptr(color));

		GLUquadric *q = gluNewQuadric();
		gluSphere(q, 1.0f, 64, 64);
		gluDeleteQuadric(q);

		/*glBegin(GL_TRIANGLES);

		glVertex3f(  0.0f,  0.0f,  0.0f);
		glVertex3f(  1.0f,  0.0f,  0.0f);
		glVertex3f(  0.0f,  1.0f,  0.0f);

		glEnd();*/
	}

		break;

	case RenderableObject::CUBE:
		break;

	}
}