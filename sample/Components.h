#pragma once

#include "gl.h"

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "BehaviourManager.h"
#include "Keyboard.h"

extern Keyboard       g_keyboard;
extern luapp11::State g_state;

enum ComponentType
{
	COMPONENT_TRANSFORM,
	COMPONENT_RENDER,
	COMPONENT_BEHAVIOUR,
	COMPONENT_DATA,
	COMPONENT_KEYBOARD
};

class DynamicObject;

class Component
{

public:

	Component(ComponentType type) :
		m_componentType(type),
		m_owner(nullptr)
	{

	}

	virtual ~Component(void) { }

	ComponentType GetComponentType(void) const
	{
		return m_componentType;
	}

protected:

	DynamicObject *m_owner;

private:

	ComponentType m_componentType;

	friend class DynamicObject;

};

struct TransformComponent :
	public Component
{

	TransformComponent(void) :
		Component(COMPONENT_TRANSFORM),
		position(0),
		scaling(1)
	{

	}

	glm::mat4 GetTransformMatrix(void) const
	{

		return glm::toMat4(orientation) *
			scaling *
			glm::translate(glm::mat4(1.0f), position);

	}

	glm::vec3 position;
	float     scaling;
	glm::quat orientation;

};
struct BehaviourComponent :
	public Component
{

	BehaviourComponent(const char *name) :
		Component(COMPONENT_BEHAVIOUR)
	{
		behaviour = BehaviourManager::GetSingleton()->Create(name);
	}

	luapp11::LuaBehaviour behaviour;

};

enum class RenderableObject {
	NONE,
	SPHERE,
	CUBE
};

struct DataComponent :
	public Component
{

	DataComponent(void) :
		Component(COMPONENT_DATA),
		sref(new luapp11::RegistryReference)
	{
		lua_newtable(g_state.GetState());
		sref->Create(g_state.GetState(), -1);
		lua_pop(g_state.GetState(), 1);
	}

	luapp11::SharedRegistryReference sref;

};

struct KeyboardComponent :
	public Component, public KeyboardListener
{

	KeyboardComponent(void) :
		Component(COMPONENT_KEYBOARD)
	{
		g_keyboard.AddKeyboardListener(this);
	}

	~KeyboardComponent(void)
	{
		g_keyboard.RemoveKeyboardListener(this);
	}

	void OnKeyPress(KeyboardKey key)
	{

		if (m_owner)
		{
			luapp11::Object object(g_state, m_owner);
			object.Call<void>("OnKeyPress", std::forward<KeyboardKey>(key));
		}

	}

	void OnKeyRelease(KeyboardKey key)
	{

		if (m_owner)
		{
			luapp11::Object object(g_state, m_owner);
			object.Call<void>("OnKeyRelease", std::forward<KeyboardKey>(key));
		}

	}

};

struct RenderComponent :
	public Component
{

	RenderComponent(void) :
		Component(COMPONENT_RENDER),
		color(1, 1, 1),
		type(RenderableObject::NONE) { }

	void Render(void);

	RenderableObject type;
	glm::vec3        color;

};