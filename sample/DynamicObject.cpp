#include "DynamicObject.h"

#include <lua++11/lua++11.h>

int DataComponentIndex(lua_State *L)
{

	luapp11::Object obj(luapp11::FromStack(L, 1));

	DataComponent *dc = obj.Cast<DataComponent*>();

	luapp11::Push(*dc->sref);
	lua_pushvalue(L, 2);
	lua_gettable(L, -2);

	return 1;

}

int DataComponentNewIndex(lua_State *L)
{

	luapp11::Object obj(luapp11::FromStack(L, 1));

	DataComponent *dc = obj.Cast<DataComponent*>();

	luapp11::Push(*dc->sref);
	lua_pushvalue(L, 2);
	lua_pushvalue(L, 3);

	lua_settable(L, -3);

	return 0;

}