#pragma once

#include <map>
#include <string>

#include <lua++11/lua++11.h>

#include "Components.h"

extern luapp11::State g_state;

class DynamicObject
{

public:

	DynamicObject(void)
	{
		m_components["transform"] = new TransformComponent;
	}

	DynamicObject(const char *name) :
		m_name(name)
	{
		m_components["transform"] = new TransformComponent;
	}

	~DynamicObject(void)
	{

		for (auto p : m_components)
		{
			delete p.second;
		}

	}

	Component* GetComponent(const char *name)
	{

		auto it = m_components.find(name);

		if (it != m_components.end())
		{
			return it->second;
		}

		return nullptr;

	}

	const Component * GetComponent(const char *name) const
	{

		auto it = m_components.find(name);

		if (it != m_components.end())
		{
			return it->second;
		}

		return nullptr;

	}

	TransformComponent* GetTransformComponent(void)
	{
		return static_cast<TransformComponent*>(GetComponent("transform"));
	}

	DataComponent * GetDataComponent(void)
	{
		return static_cast<DataComponent*>(GetComponent("data"));
	}

	KeyboardComponent * GetKeyboardComponent(void)
	{
		return static_cast<KeyboardComponent*>(GetComponent("keyboard"));
	}

	RenderComponent * GetRenderComponent(void)
	{
		return static_cast<RenderComponent*>(GetComponent("render"));
	}

	const BehaviourComponent * GetBehaviourComponent(void) const
	{
		return static_cast<const BehaviourComponent*>(GetComponent("behaviour"));
	}

	Component * SetComponent(const char *name, Component * && c)
	{

		Component *old = GetComponent(name);

		if (old)
		{
			old->m_owner = nullptr;
		}

		m_components[name] = c;
		c->m_owner = this;

		return old;

	}

	const char* GetName(void) const
	{
		return m_name.c_str();
	}

	void Update(float time)
	{
		luapp11::Object o(g_state, this);

		try
		{
			o.Call<void>("Update", std::forward<float>(time));
		}
		catch (luapp11::LuaException e)
		{
		}
		
	}

private:

	std::map<std::string, Component*> m_components;
	std::string m_name;

};

int DataComponentIndex(lua_State *L);
int DataComponentNewIndex(lua_State *L);

namespace luapp11
{

	template <>
	struct LuaBehaviourConverter<DynamicObject>
	{
		static LuaBehaviour GetBehaviour(const DynamicObject *object) { return object->GetBehaviourComponent()->behaviour; }
	};

}