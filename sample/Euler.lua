return function ( x0, v0, a, dt )

	local x = x0 + v0 * dt
	local v = v0 + a * dt
	
	return { position = x, velocity = v }

end