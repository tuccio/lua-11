#pragma once

#include <lua++11/lua++11.h>

#include "Keyboard.h"
#include "ObjectsManager.h"

extern luapp11::State g_state;
extern ObjectsManager g_objectsManager;
extern Keyboard       g_keyboard;