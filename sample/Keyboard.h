#pragma once

#include <vector>

enum class KeyboardKey
{

	ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE,

	A = 'a', B, C, D, E, F, G, H, I, J, K, L,
	M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

	LEFT, RIGHT, UP, DOWN

};

struct KeyboardListener
{
	virtual void OnKeyPress(KeyboardKey key) = 0;
	virtual void OnKeyRelease(KeyboardKey key) = 0;
};

class Keyboard
{

public:

	void AddKeyboardListener(KeyboardListener *object)
	{
		m_listeners.push_back(object);
	}

	void RemoveKeyboardListener(KeyboardListener *object)
	{

		auto it = std::find(m_listeners.begin(), m_listeners.end(), object);

		if (it != m_listeners.end())
		{
			m_listeners.erase(it);
		}

	}

	void Press(KeyboardKey key)
	{
		for (KeyboardListener *kl : m_listeners)
		{
			kl->OnKeyPress(key);
		}
	}

	void Release(KeyboardKey key)
	{
		for (KeyboardListener *kl : m_listeners)
		{
			kl->OnKeyRelease(key);
		}
	}

private:

	std::vector<KeyboardListener*> m_listeners;

};