#include "ObjectsManager.h"


ObjectsManager::ObjectsManager(void)
{
}


ObjectsManager::~ObjectsManager(void)
{
}

DynamicObject* ObjectsManager::CreateObject(const char *name)
{

	DestroyObject(name);

	DynamicObject *newObject = new DynamicObject(name);

	m_objects[name] = newObject;

	return newObject;


}

DynamicObject* ObjectsManager::CreateEntity(const char *name, const char *behaviour)
{

	DynamicObject      *obj = CreateObject(name);
	BehaviourComponent *b   = new BehaviourComponent(behaviour);

	obj->SetComponent("behaviour", std::move(b));

	luapp11::Object luaObject(g_state.GetState(), obj);

	luaObject.Call<void>("Init");

	return obj;


}

DynamicObject* ObjectsManager::GetObject(const char *name)
{

	auto it = m_objects.find(name);

	if (it != m_objects.end())
	{
		return it->second;
	}

	return nullptr;

}

void ObjectsManager::DestroyObject(const char *name)
{

	auto it = m_objects.find(name);

	if (it != m_objects.end())
	{
		delete it->second;
		m_objects.erase(it);
	}

}