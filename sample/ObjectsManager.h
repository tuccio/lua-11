#pragma once

#include <map>
#include <string>

#include "DynamicObject.h"

class ObjectsManager
{

public:

	ObjectsManager(void);
	~ObjectsManager(void);

	DynamicObject* CreateObject(const char *name);
	DynamicObject* CreateEntity(const char *name, const char *behaviour);

	DynamicObject* GetObject(const char *name);
	void           DestroyObject(const char *name);

	typedef std::map<std::string, DynamicObject*>::const_iterator Iterator;

	Iterator begin(void) const
	{
		return m_objects.begin();
	}

	Iterator end(void) const
	{
		return m_objects.end();
	}

private:

	std::map<std::string, DynamicObject*> m_objects;

};

