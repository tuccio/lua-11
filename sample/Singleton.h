#pragma once

template <typename T>
struct Singleton
{

	Singleton(void)
	{

		if (s_singleton)
		{
			throw "Attempting to instantiate singleton twice";
		}
		else
		{
			s_singleton = static_cast<T*>(this);
		}

	}

	~Singleton(void)
	{
		s_singleton = nullptr;
	}

	static T* GetSingleton(void)
	{
		return s_singleton;
	}

private:

	static T *s_singleton;

};

template <typename T>
T* Singleton<T>::s_singleton = nullptr;