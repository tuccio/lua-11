#pragma once

#ifdef _WIN32
#include <Windows.h>
#undef GetObject
#endif

#include <gl/GL.h>
#include <gl/GLU.h>

#include <gl/glut.h>