#include <lua++11/lua++11.h>

#include "BindGLM.h"
#include "BindComponents.h"
#include "DynamicObject.h"
#include "Globals.h"
#include "ObjectsManager.h"

#include "gl.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#define UPDATE_TIME_SPAN 12

void ReshapeFunc(int, int);
void DisplayFunc(void);
void UpdateFunc(int);
void KeyboardFunc(unsigned char, int, int);
void KeyboardUpFunc(unsigned char, int, int);
void SpecialFunc(int, int, int);
void SpecialUpFunc(int, int, int);

template <typename T>
void PrintVector(const std::vector<T> &v)
{
	for (const T &e : v)
	{
		std::cout << e << std::endl;
	}
}

void PrintString(std::string s)
{
	std::cout << s << std::endl;
}

int main(int argc, char *argv[])
{

	using namespace luapp11;

	g_state.Create();
	g_state.OpenLibraries();

	BehaviourManager behaviourManager(g_state.GetState());

	BindGLM(g_state);
	BindComponents(g_state);

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

	glutInitWindowSize(1280, 720);
	glutCreateWindow("Sample");

	glutReshapeFunc(&ReshapeFunc);
	glutDisplayFunc(&DisplayFunc);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	if (!g_state.RunFile("sample.lua"))
	{
		std::cout << "Cannot run sample.lua" << std::endl;
	}

	glutKeyboardFunc(&KeyboardFunc);
	glutKeyboardUpFunc(&KeyboardUpFunc);

	glutSpecialFunc(&SpecialFunc);
	glutSpecialUpFunc(&SpecialUpFunc);

	glutTimerFunc(UPDATE_TIME_SPAN, &UpdateFunc, 0);

	glutMainLoop();

	return 0;

}

void ReshapeFunc(int width, int height)
{

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0f, (float) width / height, 0.0f, 100.0f);

}

void DisplayFunc(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (DynamicObject *camera = g_objectsManager.GetObject("camera"))
	{

		TransformComponent *cameraTransform = camera->GetTransformComponent();


		glm::mat4 w2c = glm::inverse(cameraTransform->GetTransformMatrix());
		glLoadMatrixf(glm::value_ptr(w2c));

	}

	for (auto p : g_objectsManager)
	{

		TransformComponent *tc = p.second->GetTransformComponent();
		RenderComponent *rc = p.second->GetRenderComponent();

		if (tc && rc)
		{

			glPushMatrix();

			glm::mat4 w = tc->GetTransformMatrix();
			glMultMatrixf(glm::value_ptr(w));

			rc->Render();

			glPopMatrix();

		}

	}

	glutSwapBuffers();

}

void UpdateFunc(int param)
{

	float time = UPDATE_TIME_SPAN / 1000.0f;

	for (auto p : g_objectsManager)
	{
		p.second->Update(time);
	}

	glutPostRedisplay();

	glutTimerFunc(UPDATE_TIME_SPAN, &UpdateFunc, param);

}

void KeyboardFunc(unsigned char key, int, int)
{

	if (key >= 'a' && key <= 'z')
	{
		KeyboardKey vk = static_cast<KeyboardKey>(static_cast<int>(KeyboardKey::A) + (key - 'a'));
		g_keyboard.Press(vk);
	}

}

void KeyboardUpFunc(unsigned char key, int, int)
{

	if (key >= 'a' && key <= 'z')
	{
		KeyboardKey vk = static_cast<KeyboardKey>(static_cast<int>(KeyboardKey::A) + (key - 'a'));
		g_keyboard.Release(vk);
	}

}

void SpecialFunc(int key, int, int)
{

	switch (key)
	{

	case GLUT_KEY_LEFT:
		g_keyboard.Press(KeyboardKey::LEFT);
		break;

	case GLUT_KEY_RIGHT:
		g_keyboard.Press(KeyboardKey::RIGHT);
		break;

	case GLUT_KEY_UP:
		g_keyboard.Press(KeyboardKey::UP);
		break;

	case GLUT_KEY_DOWN:
		g_keyboard.Press(KeyboardKey::DOWN);
		break;

	}

}

void SpecialUpFunc(int key, int, int)
{

	switch (key)
	{

	case GLUT_KEY_LEFT:
		g_keyboard.Release(KeyboardKey::LEFT);
		break;

	case GLUT_KEY_RIGHT:
		g_keyboard.Release(KeyboardKey::RIGHT);
		break;

	case GLUT_KEY_UP:
		g_keyboard.Release(KeyboardKey::UP);
		break;

	case GLUT_KEY_DOWN:
		g_keyboard.Release(KeyboardKey::DOWN);
		break;

	}

}