#include <lua++11/Class.h>
#include <lua++11/ClassRegistry.h>
#include <lua++11/Debug.h>

#include <cstring>

namespace luapp11
{

	namespace detail
	{

		static LUAPP11_INLINE bool BehaviourLookup(lua_State *L)
		{

			bool behaviourLookupResult = false;

			lua_getuservalue(L, 1);

			if (lua_type(L, -1) != LUA_TNIL)
			{
				lua_pushvalue(L, 2);
				lua_gettable(L, -2);

				if (lua_type(L, -1) != LUA_TNIL)
				{
					lua_replace(L, -2);
					behaviourLookupResult = true;
				}
				else
				{
					lua_pop(L, 2);
				}

			}
			else
			{
				lua_pop(L, 1);
			}

			return behaviourLookupResult;

		}

		static LUAPP11_INLINE int CustomIndexLookup(lua_State *L, ClassRegistryInfo *cr)
		{

			int returnValues = 0;
			Push(cr->metatable);

			lua_rawgeti(L, -1, LUAPP11_CLASSMT_CUSTOMINDEX);
			
			if (lua_type(L, -1) != LUA_TFUNCTION)
			{
				lua_pop(L, 2);
			}
			else
			{
				lua_CFunction customIndex = lua_tocfunction(L, -1);
				lua_pop(L, 2);
				returnValues = customIndex(L);
			}

			return returnValues;
			
		}

		static LUAPP11_INLINE bool CustomNewIndexLookup(lua_State *L, ClassRegistryInfo *cr)
		{

			bool hasCustomNewIndex = false;
			Push(cr->metatable);

			lua_rawgeti(L, -1, LUAPP11_CLASSMT_CUSTOMNEWINDEX);

			if (lua_type(L, -1) != LUA_TFUNCTION)
			{
				lua_pop(L, 2);
			}
			else
			{
				lua_CFunction customNewIndex = lua_tocfunction(L, -1);
				lua_pop(L, 2);
				customNewIndex(L);
				hasCustomNewIndex = true;
			}

			return hasCustomNewIndex;

		}

		static enum ClassMetatableLookupResult
		{
			LUAPP11_CLASS_LOOKUP_RESULT_NONE,
			LUAPP11_CLASS_LOOKUP_RESULT_PROPERTY,
			LUAPP11_CLASS_LOOKUP_RESULT_FUNCTION,
			LUAPP11_CLASS_LOOKUP_RESULT_UNSPECIFIED
		};

		static ClassMetatableLookupResult ClassMetatableLookup(lua_State *L, ClassRegistry *cr, ClassRegistryInfo *classReg)
		{

			// Stack: userdata, field name, class metatable

			int udIndex = lua_absindex(L, -3);
			int fnIndex = lua_absindex(L, -2);
			int mtIndex = lua_absindex(L, -1);

			lua_rawgeti(L, mtIndex, LUAPP11_CLASSMT_PROPERTIES);

			//LUAPP11_DEBUG_TABLE_DUMP(L, -1);
			
			// Stack: userdata, field name, class metatable, class properties

			lua_pushvalue(L, fnIndex);

			//LUAPP11_DEBUG_STACK_DUMP(L)

			lua_rawget(L, -2);

			if (lua_isnil(L, -1))
			{

				lua_pushvalue(L, -4);
				lua_rawget(L, -4);

				if (lua_isnil(L, -1))
				{

					for (auto kv : classReg->upcastMap)
					{

						ClassID baseClassId = kv.first;

						if (baseClassId != classReg->cid)
						{

							lua_settop(L, mtIndex);

							lua_pushvalue(L, udIndex);
							lua_pushvalue(L, fnIndex);

							ClassRegistryInfo *baseClassReg = cr->GetClass(baseClassId);
							Push(baseClassReg->metatable);

							auto lookupResult = ClassMetatableLookup(L, cr, baseClassReg);

							if (lookupResult != LUAPP11_CLASS_LOOKUP_RESULT_NONE)
							{

								lua_replace(L, udIndex);
								lua_settop(L, udIndex);

								return lookupResult;

							}
						}

					}

				}
				else
				{

					// A field was found in the table
					return LUAPP11_CLASS_LOOKUP_RESULT_UNSPECIFIED;

				}

			}
			else
			{

				lua_replace(L, udIndex);
				lua_settop(L, udIndex);

				// Property found
				return LUAPP11_CLASS_LOOKUP_RESULT_PROPERTY;

			}


			return LUAPP11_CLASS_LOOKUP_RESULT_NONE;

		}

		int ClassIndexMetafunction(lua_State *L)
		{

			LUAPP11_DEBUG_HALT_IF(strcmp(lua_tostring(L, 2), "__eq") == 0)
			LUAPP11_DEBUG_HALT_IF(strcmp(lua_tostring(L, 2), "__unm") == 0)

			ClassRegistryInfo *classReg;
			ClassRegistry *cr = ClassRegistry::GetInstance(L);

			int returnValues = 0;

			// Stack: userdata, field name

			lua_getmetatable(L, 1);

			lua_rawgeti(L, -1, LUAPP11_CLASSMT_CLASSID);

			ClassID cid = lua_tointeger(L, -1);

			lua_pop(L, 1);

			classReg = cr->GetClass(cid);

			lua_pushvalue(L, 1);
			lua_pushvalue(L, 2);
			Push(classReg->metatable);

			auto lookupResult = ClassMetatableLookup(L, cr, classReg);

			if (lookupResult == LUAPP11_CLASS_LOOKUP_RESULT_PROPERTY)
			{

				LuaObjectWrapper * o = static_cast<LuaObjectWrapper*>(lua_touserdata(L, 1));

				if (!o->IsConst())
				{
					
					lua_rawgeti(L, -1, LUAPP11_PROPERTY_GET);

					if (lua_isnil(L, -1))
					{
						lua_rawgeti(L, -2, LUAPP11_PROPERTY_CGET);
						lua_replace(L, -2);
					}

					lua_pushvalue(L, 1);
					lua_call(L, 1, 1);

				}
				else
				{
					lua_rawgeti(L, -1, LUAPP11_PROPERTY_CGET);
					lua_pushvalue(L, 1);
					lua_call(L, 1, 1);
				}

				returnValues = 1;

			}
			else if (lookupResult == LUAPP11_CLASS_LOOKUP_RESULT_NONE)
			{

				lua_settop(L, 3);

				if (!BehaviourLookup(L))
				{

					lua_settop(L, 2);
					returnValues = CustomIndexLookup(L, classReg);

				}
				else
				{
					returnValues = 1;
				}

			}
			else
			{
				returnValues = 1;
			}

			return returnValues;

		}

		int ClassNewIndexMetafunction(lua_State *L)
		{

			//LUAPP11_DEBUG_STACK_DUMP(L);

			//LUAPP11_DEBUG_HALT_IF((!strcmp(lua_tostring(L, 2), "position")))

			ClassRegistryInfo *classReg;
			ClassRegistry *cr = ClassRegistry::GetInstance(L);

			// Stack: ..., userdata, field name

			lua_getmetatable(L, 1);

			lua_rawgeti(L, -1, LUAPP11_CLASSMT_CLASSID);

			ClassID cid = lua_tointeger(L, -1);

			lua_pop(L, 1);

			classReg = cr->GetClass(cid);

			lua_pushvalue(L, 1);
			lua_pushvalue(L, 2);
			Push(classReg->metatable);

			auto lookupResult = ClassMetatableLookup(L, cr, classReg);

			switch (lookupResult)
			{

			case LUAPP11_CLASS_LOOKUP_RESULT_PROPERTY:

				lua_rawgeti(L, -1, LUAPP11_PROPERTY_SET);

				if (lua_isnil(L, -1))
				{

					std::stringstream ss;
					ss << "Trying to set read-only property '" << lua_tostring(L, 2) << "' in class " << classReg->name << ".";
					luaL_error(L, ss.str().c_str());

				}
				else
				{

					lua_pushvalue(L, 1);
					lua_pushvalue(L, 3);

					lua_call(L, 2, 0);

				}
				

				break;

			case LUAPP11_CLASS_LOOKUP_RESULT_UNSPECIFIED:

				// what to do ?
				assert(false);
				break;

			case LUAPP11_CLASS_LOOKUP_RESULT_NONE:

				if (!CustomNewIndexLookup(L, classReg))
				{
					std::stringstream ss;
					ss << "Trying to access unknown field '" << lua_tostring(L, 2) << "' in class " << classReg->name << ".";
					luaL_error(L, ss.str().c_str());
				}

				break;

			default:

				break;

			}

			return 0;

		}

	}

}