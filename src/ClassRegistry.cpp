#include <lua++11/ClassRegistry.h>
#include <lua++11/Defines.h>

#include <deque>

using namespace luapp11::detail;

ClassRegistry::ClassRegistry(void) :
m_lua(nullptr)
{
}


ClassRegistry::~ClassRegistry(void)
{
}

bool ClassRegistry::Create(lua_State *L)
{

	if (m_lua)
	{
		return false;
	}

	m_lua = L;

	lua_pushstring(m_lua, LUAPP11_REGKEY_CLASSREGISTRY);
	lua_pushlightuserdata(m_lua, static_cast<void*>(this));
	lua_rawset(m_lua, LUA_REGISTRYINDEX);

	return true;

}

void ClassRegistry::Destroy(void)
{

	m_classes.clear();
	m_lua = nullptr;

}

ClassRegistry* ClassRegistry::GetInstance(lua_State *L)
{
	
	void *p = nullptr;

	lua_pushstring(L, LUAPP11_REGKEY_CLASSREGISTRY);
	lua_rawget(L, LUA_REGISTRYINDEX);

	assert(lua_type(L, -1) == LUA_TLIGHTUSERDATA);

	p = lua_touserdata(L, -1);	
	lua_pop(L, 1);

	return static_cast<ClassRegistry*>(p);

}

/*
bool ClassRegistry::IsBaseOf(ClassID base, ClassID derived) const
{

	/*const ClassRegistryInfo *classReg = GetClass(derived);

	for (ClassID baseClass : classReg->baseClasses)
	{

	if (baseClass == base || IsBaseOf(base, baseClass))
	{
	return true;
	}

	}

	return false;

	return true;

}*/

bool ClassRegistry::GetPathToBase(ClassID base, ClassID derived, std::deque<ClassID> *path)
{

	if (base == derived)
	{
		return true;
	}

	ClassRegistryInfo *classReg = GetClass(derived);

	for (auto kv : classReg->upcastMap)
	{

		if (kv.first != derived)
		{

			if (GetPathToBase(base, kv.first, path))
			{

				if (path && derived != kv.first)
				{
					path->push_front(kv.first);
				}

				return true;
			}

		}

	}

	return false;

}

ClassID luapp11::detail::GetNextID(void)
{

	static ClassID id = 0;

	return ++id;

}