#include <lua++11/Constructor.h>

namespace luapp11
{

	namespace detail
	{

		int ConstructorChooser(lua_State *L)
		{

			int top = lua_gettop(L);
			int nargs = top - 1;

			int ctorTableIndex = lua_upvalueindex(1);

			//LUAPP11_DEBUG_TABLE_DUMP(L, ctorTableIndex);

			int constructors = lua_rawlen(L, ctorTableIndex);

			for (int i = 1; i <= constructors; i++)
			{

				lua_rawgeti(L, ctorTableIndex, i);
				
				lua_rawgeti(L, -1, LUAPP11_CONSTRUCTORTABLE_ARGS);
				int fargs = lua_tointeger(L, -1);

				lua_rawgeti(L, -2, LUAPP11_CONSTRUCTORTABLE_SIGNATURE);
				lua_CFunction signatureCheck = lua_tocfunction(L, -1);

				lua_pop(L, 3);

				if (nargs == fargs && signatureCheck(L))
				{

					lua_rawgeti(L, ctorTableIndex, i);
					lua_rawgeti(L, -1, LUAPP11_CONSTRUCTORTABLE_CONSTRUCTOR);
					lua_insert(L, 2);

					lua_pop(L, 1);

					// Call the function

					lua_call(L, nargs, 1);

					return 1;

				}

			}

			/*
			lua_pushvalue(L, ctorTableIndex);
			lua_pushnil(L);

			// Stack: ClassMetatable, Args ..., ClassMetatable, nil

			for (int i = 1; lua_next(L, -2); i++)
			{

				// Stack: ClassMetatable, Args ..., ClassMetatable, Key[i], ConstructorTable[i]

				lua_rawgeti(L, -1, LUAPP11_CONTABLE_ARGS);

				int fargs = lua_tointeger(L, -1);
				lua_pop(L, 1);

				if (nargs == fargs)
				{

					// Need to check signature matching

					lua_rawgeti(L, -1, LUAPP11_CONTABLE_SIGNATURE);

					lua_CFunction signatureCheck = lua_tocfunction(L, -1);
					lua_pop(L, 1);

					if (signatureCheck(L))
					{

						// Get the function and copy the arguments

						lua_rawgeti(L, -1, LUAPP11_CONTABLE_CONSTRUCTOR);

						for (int j = 2; j <= top; j++)
						{
							lua_pushvalue(L, j);
						}

						// Call the function

						lua_call(L, nargs, 1);

						// Copy the return values right after the arguments

						lua_copy(L, -1, top + 1);
						lua_settop(L, top + 1);

						return 1;

					}
					else
					{

						// Stack: ClassMetatable, Args ..., ClassMetatable, Key[i], ConstructorTable[i]
						lua_pop(L, 1);
						// Stack: ClassMetatable, Args ..., ClassMetatable, Key[i]

					}

				}
				else
				{

					// Stack: OverloadTable, Args ..., ClassMetatable, Key[i], ConstructorTable[i]
					lua_pop(L, 1);
					// Stack: OverloadTable, Args ..., ClassMetatable, Key[i]

				}

			}*/

			luaL_error(L, "No matching constructor found");

			return 0;

		}

	}

}