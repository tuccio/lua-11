#include <lua++11/Debug.h>

std::string luapp11::detail::GetStackDump(lua_State *L)
{

	std::stringstream ss;

	int n = lua_gettop(L);

	ss << "{ ";

	for (int i = 0; i < n; i++)
	{

		ss << "[" << lua_absindex(L, -(1 + i)) << "]: ";

		switch (lua_type(L, -(1 + i)))
		{

		case LUA_TNIL:
			ss << "nil";
			break;

		case LUA_TNUMBER:
			ss << "number<" << lua_tonumber(L, -(1 + i)) << ">";
			break;

		case LUA_TBOOLEAN:
			ss << "boolean";
			break;

		case LUA_TFUNCTION:
			ss << "function";
			break;

		case LUA_TLIGHTUSERDATA:
			ss << "lightuserdata";
			break;

		case LUA_TUSERDATA:
			ss << "userdata";
			break;

		case LUA_TTABLE:
			ss << "table";
			break;

		case LUA_TSTRING:
			ss << "string<" << lua_tostring(L, -(1 + i)) << ">";
			break;

		default:
			ss << "unknown";
			break;

		}

		ss << (i == n - 1 ? "; " : " ");

	}

	ss << "}";

	return ss.str();

}

std::string luapp11::detail::GetTableDump(lua_State *L, int index)
{

	int absIndex = lua_absindex(L, index);
	int top = lua_gettop(L);

	std::stringstream ss;

	ss << "{";

	lua_pushvalue(L, index);
	lua_pushnil(L);

	bool first = true;

	while (lua_next(L, -2))
	{

		std::string key;
		std::string value;

		if (lua_type(L, -2) == LUA_TSTRING)
		{
			key = "\"";
			key += lua_tostring(L, -2);
			key += "\"";
		}
		else
		{
			key = std::to_string(lua_tointeger(L, -2));
		}

		switch (lua_type(L, -1))
		{

		case LUA_TNIL:
			ss << "[nil]";
			break;

		case LUA_TNUMBER:
			value = std::to_string(lua_tonumber(L, -1));
			break;

		case LUA_TBOOLEAN:
			value = std::to_string(lua_toboolean(L, -1));
			break;

		case LUA_TFUNCTION:
			value = "[function]";
			break;

		case LUA_TLIGHTUSERDATA:
			value = "[lightuserdata]";
			break;

		case LUA_TUSERDATA:
			value = "[userdata]";
			break;

		case LUA_TTABLE:

			// Not safe, only avoids an infinite loop on the first recursion
			if (!lua_rawequal(L, absIndex, -1))
			{
				value = GetTableDump(L, -1);
			}

			break;

		case LUA_TSTRING:

			value = "\"";
			value += lua_tostring(L, -1);
			value += "\"";

			break;

		default:
			ss << "unknown";
			break;

		}

		if (first)
		{
			first = false;
			ss << std::endl;
		}
		else
		{
			ss << ";" << std::endl;
		}

		ss << "[" << key << "]: " << value;

		lua_pop(L, 1);

	}

	ss << std::endl << "}";

	lua_settop(L, top);

	return ss.str();

}