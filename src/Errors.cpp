#include <lua++11/Errors.h>

namespace luapp11
{

	int LuaErrorHandler(lua_State *L)
	{
#ifdef LUAPP11_VERBOSE
		NotifyError(lua_tostring(L, -1));
#endif
		return 0;
	}

}