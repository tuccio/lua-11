#include <lua++11/LuaBehaviour.h>

#include <lua++11/Debug.h>
#include <lua++11/Errors.h>

#include <cassert>
#include <cstring>

using namespace luapp11;

LuaBehaviour::LuaBehaviour(void)
{
}


LuaBehaviour::~LuaBehaviour(void)
{
}

bool LuaBehaviour::Create(lua_State *L, const char *filename, bool allowGlobalWrite)
{

	bool success = false;

	if (!luaL_loadfile(L, filename))
	{

		const char *upvalue = nullptr;

		if (!allowGlobalWrite)
		{
			
			// Create _ENV for sandbox

			lua_newtable(L);

			// Create its metatable
			lua_newtable(L);
			lua_getglobal(L, "_G");

			lua_setfield(L, -2, "__index");
			lua_setmetatable(L, -2);

			upvalue = lua_setupvalue(L, -2, 1);

		}

		if (allowGlobalWrite || upvalue)
		{

			assert(strncmp(upvalue, "_ENV", 4) == 0);

			/*lua_getupvalue(L, -1, 1);
			lua_insert(L, -2);*/

			if (!lua_pcall(L, 0, 1, 0))
			{

				success = m_ref.Create(L, -1);
				lua_pop(L, 1);

			}

		}

	}
	else
	{
		NotifyError(lua_tostring(L, -1));
		lua_pop(L, 1);
	}

	return success;

}

void LuaBehaviour::Destroy(void)
{
	m_ref.Destroy();
}