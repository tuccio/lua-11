#include <lua++11/Namespace.h>

#include <functional>

using namespace luapp11;

Namespace::Namespace(void)
{

}

Namespace::Namespace(const char *name) :
m_name(name)
{
	m_type = NAMESPACE;
	m_function = std::bind(&detail::RegistrationTree::RegisterNamespace, std::placeholders::_1, m_name, &m_tree);
}

const char* Namespace::GetName(void) const
{
	return m_name.c_str();
}

Namespace& Namespace::operator[] (detail::RegistrationTree & tree)
{
	m_tree.Attach(std::forward<detail::RegistrationTree>(tree));
	return *this;
}

Namespace& Namespace::operator[] (Registrable & r)
{
	m_tree.Attach(detail::RegistrationTree(std::forward<Registrable>(r)));
	return *this;
}

RegistrationFunctionType Namespace::GetRegistrationFunction(void)
{
	return m_function;
}