#include <lua++11/Object.h>

using namespace luapp11;

namespace luapp11
{

	namespace detail
	{

		static ClassID GetObjectCID(lua_State *L, int index)
		{

			ClassID cid = LUAPP11_CLASSID_INVALID;
			int luaType = lua_type(L, index);

			if (luaType == LUA_TUSERDATA)
			{

				lua_getmetatable(L, index);

				if (lua_type(L, -1) != LUA_TNIL)
				{
					lua_rawgeti(L, -1, LUAPP11_CLASSMT_CLASSID);
					cid = lua_tointeger(L, -1);
					lua_pop(L, 2);
				}
				else
				{
					lua_pop(L, 1);
				}

			}

			return cid;

		}

	}

}

Object::Object(void)
{

}

Object::Object(Object &object, const char *field)
{

	lua_State *L = object.GetState();
	StackCleaner sc(L);

	object.Push();
	lua_getfield(L, -1, field);

	m_luaType = lua_type(L, -1);
	m_cid     = detail::GetObjectCID(L, -1);

	RegistryReference ref;
	ref.Create(L, -1);

	m_ref = MakeSharedReference(std::move(ref));

}

Object::Object(Object && object)
{
	m_ref     = std::move(object.m_ref);
	m_luaType = object.m_luaType;
}

Object::Object(const FromStack & fs)
{

	m_luaType = lua_type(fs.L, fs.index);
	m_cid     = detail::GetObjectCID(fs.L, -1);

	RegistryReference ref;
	ref.Create(fs.L, fs.index);

	m_ref = MakeSharedReference(std::move(ref));

}

void Object::Push(void) const
{
	::Push(*m_ref);
}

void Object::SetBehaviour(const LuaBehaviour &b)
{

	assert(b.GetState() == m_ref->GetState() &&
		"Operations between different states not allowed");

	Push();
	::Push(b);

	lua_setuservalue(m_ref->GetState(), -2);

}

TableField Object::operator[] (const char *field)
{
	return TableField(*this, field);
}

/* TableField implementation */

TableField::TableField(luapp11::Object &object, const char *field) :
	Object(object, field),
	m_parent(object),
	m_field(field)
{
}