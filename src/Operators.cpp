#include <lua++11/Operators.h>

namespace luapp11
{

	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_ADD>::metafunction = "__add";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_SUB>::metafunction = "__sub";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_MUL>::metafunction = "__mul";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_DIV>::metafunction = "__div";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_MOD>::metafunction = "__mod";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_SHL>::metafunction = "__shl";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_SHR>::metafunction = "__shr";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_EQ>::metafunction  = "__eq";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_LT>::metafunction  = "__lt";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_LE>::metafunction  = "__le";


	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_BNOT>::metafunction  = "__bnot";
	const char * OperatorsWrappersGenerator<LUAPP11_OPERATOR_UNM>::metafunction   = "__unm";


}