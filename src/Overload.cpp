#include <lua++11/Overload.h>
#include <lua++11/Functions.h>
#include <lua++11/Debug.h>

#include <string>

namespace luapp11
{

	namespace detail
	{

		int OverloadChooser(lua_State *L)
		{

			//LUAPP11_DEBUG_STACK_DUMP(L)

			// Stack: OverloadTable, Args ...

			int top       = lua_gettop(L);
			int nargs     = top - 1;
			int rvalues   = -1;

			int overloads = lua_rawlen(L, 1);

			for (int i = 1; i <= overloads; i++)
			{

				lua_rawgeti(L, 1, i);

				lua_rawgeti(L, -1, LUAPP11_FUNCTIONTABLE_ARGS);
				int fargs = lua_tointeger(L, -1);

				lua_rawgeti(L, -2, LUAPP11_FUNCTIONTABLE_SIGNATURE);
				lua_CFunction signatureCheck = lua_tocfunction(L, -1);

				lua_rawgeti(L, -3, LUAPP11_FUNCTIONTABLE_RETURNVALUES);
				int frvalues = lua_tointeger(L, -1);

				lua_pop(L, 4);

				if (nargs == fargs && signatureCheck(L))
				{

					lua_rawgeti(L, 1, i);
					lua_rawgeti(L, -1, LUAPP11_FUNCTIONTABLE_FUNCTION);
					lua_replace(L, 1);

					lua_pop(L, 1);

					// Call the function

					lua_call(L, nargs, rvalues);

					rvalues = frvalues;
					break;

				}

			}

			if (rvalues < 0)
			{

				lua_rawgeti(L, 1, LUAPP11_OVERLOADTABLE_LUACFUNCTION);

				if (lua_type(L, -1) == LUA_TFUNCTION)
				{
					lua_CFunction f = lua_tocfunction(L, -1);
					lua_replace(L, 1);
					rvalues = f(L);
				}
				else
				{
					rvalues = 0;
					luaL_error(L, "No overloads matching function");
				}

			}
			
			return rvalues;

		}

	}
}