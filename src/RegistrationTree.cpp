#include <lua++11/RegistrationTree.h>
#include <lua++11/Class.h>

#include <lua++11/Enum.h>
#include <lua++11/Functions.h>
#include <lua++11/Namespace.h>
#include <lua++11/Overload.h>

using namespace luapp11;
using namespace luapp11::detail;

void RegistrationTree::Register(lua_State *L)
{

	for (auto f : m_functions)
	{
		f(L);
	}

	for (auto n : m_namespaces)
	{
		n(L);
	}

	for (auto c : m_classes)
	{
		c(L);
	}

	for (auto e : m_enums)
	{
		e(L);
	}

}

//RegistrationTree& luapp11::detail::operator, (Registrable &r, RegistrationTree &rt)
//{
//	return (rt, r);
//}

RegistrationTree& operator, (RegistrationTree &rt, Registrable & r)
{

	using namespace luapp11;
	using namespace luapp11::detail;

	switch (r.GetType())
	{

	case Registrable::FUNCTION:

	{

		IFunctionInfo *f = (IFunctionInfo*)&r;
		rt.m_functions.push_back(f->GetRegistrationFunction());
		break;
	}

	case Registrable::NAMESPACE:

	{
		Namespace *n = static_cast<Namespace*>(&r);
		rt.m_namespaces.push_back(n->GetRegistrationFunction());
		break;
	}

	case Registrable::CLASS:

	{
		IClass *c = static_cast<IClass*>(&r);
		rt.m_classes.push_back(c->GetRegistrationFunction());
		break;
	}

	case Registrable::ENUM:

	{
		IEnum *e = static_cast<IEnum*>(&r);
		rt.m_enums.push_back(e->GetRegistrationFunction());
		break;
	}

	}

	return rt;

}

RegistrationTree operator, (luapp11::Registrable & r1, luapp11::Registrable & r2)
{

	using namespace luapp11;
	using namespace luapp11::detail;

	return (RegistrationTree(std::forward<Registrable>(r1)), std::forward<Registrable>(r2));

}

void RegistrationTree::RegisterLuaFunction(lua_State *L, std::string name, lua_CFunction f)
{

	bool created = false;

	lua_pushstring(L, name.c_str());
	lua_rawget(L, -2);

	if (lua_isnil(L, -1))
	{

		lua_pop(L, 1);
		
		lua_pushstring(L, name.c_str());
		detail::PushNewOverloadTable(L);

		created = true;

	}

	lua_pushcfunction(L, f);
	lua_rawseti(L, -2, LUAPP11_OVERLOADTABLE_LUACFUNCTION);

	if (created)
	{
		lua_rawset(L, -3);
	}
	else
	{
		lua_pop(L, 1);
	}

}

void RegistrationTree::RegisterNamespace(lua_State *L, std::string name, RegistrationTree *rt)
{

	bool created = false;

	lua_pushstring(L, name.c_str());
	lua_rawget(L, -2);

	// If the namespace doesn't exist, create it

	if (lua_isnil(L, -1))
	{
		lua_pop(L, 1);
		lua_pushstring(L, name.c_str());
		lua_newtable(L);
		created = true;
	}

	rt->Register(L);

	if (created)
	{
		lua_rawset(L, -3);
	}
	else
	{
		lua_pop(L, 1);
	}

}