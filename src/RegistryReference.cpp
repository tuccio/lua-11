#include <lua++11/RegistryReference.h>

using namespace luapp11;

RegistryReference::RegistryReference(void) :
	m_state(nullptr),
	m_ref(LUA_NOREF)
{
}

RegistryReference::RegistryReference(RegistryReference && ref)
{

	m_state = ref.m_state;
	m_ref   = ref.m_ref;

	ref.Clear();

}

RegistryReference::~RegistryReference(void)
{
}

bool RegistryReference::Create(lua_State *L, int index)
{
	m_state = L;
	lua_pushvalue(m_state, index);
	m_ref = luaL_ref(m_state, LUA_REGISTRYINDEX);
	return m_ref != LUA_NOREF;
}

void RegistryReference::Destroy(void)
{

	luaL_unref(m_state, LUA_REGISTRYINDEX, m_ref);

	m_ref   = LUA_NOREF;
	m_state = nullptr;

}

void RegistryReference::Clear(void)
{
	m_state = nullptr;
	m_ref   = LUA_NOREF;
}

namespace luapp11
{
	namespace detail
	{

		struct SharedRegistryReferenceDeleter
		{

			void operator() (RegistryReference *ref) const
			{
				ref->Destroy();
				delete ref;
			}

		};

	}

	SharedRegistryReference MakeSharedReference(RegistryReference && ref)
	{
		return std::shared_ptr<RegistryReference>(
			new RegistryReference(std::forward<RegistryReference>(ref)),
			detail::SharedRegistryReferenceDeleter());
	}

}

