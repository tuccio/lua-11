#include <lua++11/Errors.h>
#include <lua++11/State.h>
#include <lua++11/Class.h>
#include <lua++11/ClassRegistry.h>
#include <lua++11/RegistrationTree.h>

#include <lua++11/Namespace.h>
#include <lua++11/Functions.h>

using namespace luapp11;

State::State(void) :
	m_lua(nullptr)
{
}


State::~State(void)
{
}

bool State::Create(void)
{

	if (!m_lua)
	{
		
		m_lua = luaL_newstate();

		detail::ClassRegistry *classRegistry = new detail::ClassRegistry();

		if (!classRegistry->Create(m_lua))
		{

			delete classRegistry;
			lua_close(m_lua);

			m_lua = nullptr;

		}
		else
		{

			lua_atpanic(m_lua, &LuaErrorHandler);

		}

	}

	return m_lua != nullptr;

}

void State::Destroy(void)
{

	if (m_lua)
	{

		detail::ClassRegistry::GetInstance(m_lua)->Destroy();
		lua_close(m_lua);

		m_lua = nullptr;

	}

}

bool State::RunCode(const char *code)
{

	bool success = false;

	if (code)
	{

		int loadResult = luaL_loadstring(m_lua, code);

		switch (loadResult)
		{

		case 0:
			lua_call(m_lua, 0, 0);
			success = true;
			break;

		case LUA_ERRSYNTAX:
			lua_error(m_lua);
			break;

		default:
			break;

		}

	}

	return success;

}

bool State::RunFile(const char *file)
{

	bool success = false;

	if (file)
	{

		int loadResult = luaL_loadfile(m_lua, file);

		switch (loadResult)
		{

		case 0:
			lua_call(m_lua, 0, 0);
			success = true;
			break;

		case LUA_ERRSYNTAX:
			lua_error(m_lua);
			break;

		default:
			break;

		}
		
	}

	return success;

}



void State::OpenLibrary(Library library)
{

	const char   *libname = nullptr;
	lua_CFunction libopen = nullptr;

	switch (library)
	{

	case Library::base:
		libopen = &luaopen_base;
		libname = "";
		break;

	case Library::coroutine:
		libopen = &luaopen_coroutine;
		libname = LUA_COLIBNAME;
		break;

	case Library::table:
		libopen = &luaopen_table;
		libname = LUA_TABLIBNAME;
		break;

	case Library::io:
		libopen = &luaopen_io;
		libname = LUA_IOLIBNAME;
		break;

	case Library::os:
		libopen = &luaopen_os;
		libname = LUA_OSLIBNAME;
		break;

	case Library::string:
		libopen = &luaopen_string;
		libname = LUA_STRLIBNAME;
		break;

	case Library::math:
		libopen = &luaopen_math;
		libname = LUA_MATHLIBNAME;
		break;

	case Library::debug:
		libopen = &luaopen_debug;
		libname = LUA_DBLIBNAME;
		break;

	case Library::package:
		libopen = &luaopen_package;
		libname = LUA_LOADLIBNAME;
		break;

	}

	luaL_requiref(m_lua, libname, libopen, 1);
	lua_pop(m_lua, 1);

}

void State::OpenLibraries(std::initializer_list<Library> libraries)
{
	for (Library lib : libraries)
	{
		OpenLibrary(lib);
	}
}

void State::OpenLibraries(void)
{
	OpenLibraries({
		Library::base,
		Library::coroutine,
		Library::table,
		Library::io,
		Library::os,
		Library::string,
		Library::math,
		//Library::debug,
		Library::package
	});
}

void State::operator [] (detail::RegistrationTree & rt)
{

	lua_pushglobaltable(m_lua);

	rt.Register(m_lua);

	lua_pop(m_lua, 1);

}

void State::operator [] (Registrable & r)
{

	(*this)[detail::RegistrationTree(std::forward<Registrable>(r))];

}

Object State::GetGlobalTable(void)
{
	StackCleaner sc(m_lua);
	lua_pushglobaltable(m_lua);
	return Object(FromStack(m_lua, -1));
}

void State::RegisterPrimitiveTypes(void)
{

	

}