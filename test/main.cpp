#include <iostream>
#include <deque>

#include "../lua++11/lua++11.h"

#define DEBUG_TEST_CRITICAL(CONDITION, OK_MSG, ERROR_MSG) \
if (!(CONDITION)) { std::cout << ERROR_MSG << std::endl; return 1; } else std::cout << OK_MSG << std::endl;

using namespace luapp11;

void halt(void)
{
	if (pow(1, 1)) return;
}

int simple(lua_State *L)
{
	std::cout << "So simple!" << std::endl;
	return 0;
}

void test1(void)
{
	std::cout << "Test 1 OK" << std::endl;
}

void test2(float x)
{
	std::cout << "Argument: " << x << std::endl;
}

void test3(float a, float b)
{
	std::cout << "Product: " << a * b << std::endl;
}

double test4(double a, double b)
{
	return a + b;
}

void test5(const char *txt)
{
	std::cout << "String overload: " << txt << std::endl;
}

void test5(float x)
{
	std::cout << "Float overload: " << x << std::endl;
}

float test5(float a, float b)
{
	return a / b;
}

void test5(float a, const char *b)
{
	std::cout << "Tricky overload" << std::endl;
}

void reference_test(int &x)
{
	x = 1337;
}

struct TestStruct
{

	TestStruct(void) :
		v(0)
	{

	}

	TestStruct(float x) :
		v(x)
	{

	}

	float Test(void)
	{
		return v;
	}

	float Test2(float x)
	{
		return v + x;
	}

	float Test3(float x) const
	{
		return v + x;
	}

	static float TestStatic(float x)
	{
		return x * 0.5f;
	}

	float v;

};

template <typename T>
class Stack
{

public:

	void Push(T v)
	{
		m_stack.push_front(v);
	}

	T Pop(void)
	{
		T v = m_stack.back();
		m_stack.pop_back();
		return v;
	}

	bool IsEmpty(void) const
	{
		return m_stack.empty();
	}

private:

	std::deque<T> m_stack;

};

int main(int argc, char *argv[])
{

	State state;

	DEBUG_TEST_CRITICAL(state.Create(), "Lua state opened", "Failed to create state");

	state.OpenLibraries();

	int n = 1337;

	auto rw1 = WrapReference(n);
	auto rw2 = WrapReference(&n);

	rw1.Get() = 666;
	rw2.Get()--;

	int *p = &rw1.Get();

	(*p)--;

	std::cout << rw1.Get() << std::endl;
	std::cout << rw2.Get() << std::endl;


	state [

		Function("halt", &halt),

		Namespace("T1")
		[
			Function("test1", &test1),
			Function("test2", &test2),
			Function("test3", &test3),
			Function("test4", &test4)
		],

		Namespace("T2")
		[
			Function("test5", (void  (*) (const char*)) &test5),
			Function("test5", (void  (*) (float)) &test5),
			Function("test5", (float (*) (float, float)) &test5),
			Function("test5", (void  (*) (float, const char*)) &test5)
		],

		Namespace("T3")
		[
			Function("halve", std::function<float(float)>(std::bind((float (*) (float, float)) &test5, std::placeholders::_1, 2)))
		],

		Class<Stack<int>>("IntStack").
			Constructor<>().
			Function("Push", &Stack<int>::Push).
			Function("Pop", &Stack<int>::Pop).
			Function("IsEmpty", &Stack<int>::IsEmpty)		

	];

	state.RunFile("test1.lua");
	state.RunFile("test2.lua");

	std::string line;

	while (!std::cin.eof())
	{

		std::getline(std::cin, line);
		
		if (!line.empty())
		{
			state.RunCode(line.c_str());
		}

	}

	state.Destroy();

	return 0;

}