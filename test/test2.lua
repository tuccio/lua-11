--halt()

--local v = vec3(1, 33, 7)

--print('x:', v:GetX())

--local printVector3 = function (v) print(v:GetX()) end
--printVector3(v)

local s = IntStack()

for i = 1, 10 do
	s:Push(i)
end

print(s:Pop())

while not s:IsEmpty() do
	print(s:Pop())
end
