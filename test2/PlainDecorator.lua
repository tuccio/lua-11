local TextHelper = require "TextHelper"
local PlainDecorator = { }

PlainDecorator.__index = TextHelper
setmetatable(PlainDecorator, PlainDecorator)

-- Methods

function PlainDecorator:Decorate()
	return self:GetText()
end

return PlainDecorator