local TextHelper = { }

function TextHelper:GetText()
	return self.text
end

function TextHelper:SetText(t)
	self.text = t
end

return TextHelper