local TextHelper = require "TextHelper"
local UpperDecorator = { }

UpperDecorator.__index = TextHelper
setmetatable(UpperDecorator, UpperDecorator)

-- Methods

function UpperDecorator:Decorate()
	return string.upper(self:GetText())
end

return UpperDecorator