local i = Integer(2.5)
local f = Float(2.5)

local intSum = i:Add(i)
local floatSum = f:Add(f)

print("int sum: " .. intSum:Get())
print("float sum: " .. floatSum:Get())

local leet = Integer(1337)

print("leet: " .. leet:Get())
leet:Set(665)
IncrementInt(leet)
print("leet: " .. leet:Get())

local gi = GetGlobalInt()

print(gi:Get())

IncrementInt(gi, 122)
IncrementGlobalInt()

print(gi:Get())

gi = i:Add(i)

print(gi:Get())
print(GetGlobalInt():Get())