#include <iostream>

#include <lua++11/lua++11.h>

using namespace luapp11;

LuaBehaviour g_cPlainDecorator;
LuaBehaviour g_cUpperDecorator;

struct Text
{

	Text(void) { }
	Text(const char *text) : text(text) { }

	std::string text;

};

int GetPlainDecorator(lua_State *L)
{
	Object o(L, Text("Hello, world!"));
	o.SetBehaviour(g_cPlainDecorator);
	o.Push();
	return 1;
}

int GetUpperDecorator(lua_State *L)
{
	Object o(L, Text("Hello, world!"));
	o.SetBehaviour(g_cUpperDecorator);
	o.Push();
	return 1;
}

struct B
{

	B(void) : b(-1) { }

	virtual ~B(void)
	{
		std::cout << "B destructor" << std::endl;
	}

	virtual void Print(void) { std::cout << "(" << b <<  ")" << std::endl; }

	int b;
};

struct D :
	public B
{

	D(void) : d(-2) { }

	~D(void)
	{
		std::cout << "D destructor" << std::endl;
	}

	void Print(void) { std::cout << "(" << b << ", " << d << ")" << std::endl; }

	float d;
};

std::shared_ptr<D> g_derived;
int g_x = 999;

Text g_text = "global";

template <typename F>
void Test(F f)
{
	std::cout << "TEST: " << detail::IsFunction<F>::value << std::endl;
}

int main(int argc, char *argv[])
{

	State state;

	state.Create();

	state.OpenLibraries();

	
	Test(&GetPlainDecorator);

	state
	[

		Class<Text>("Text").

			Constructor<>().
			Constructor<const char*>().

			Property("text", &Text::text),

		Function("GetPlainDecorator", &GetPlainDecorator),
		Function("GetUpperDecorator", &GetUpperDecorator),

		Class<B>("B").
			Function("Print", &B::Print).
			Property("b", &B::b),

		Class<D, B>("D").
			Property("d", &D::d),

		Function("GetGlobalText", std::function<Text*(void)>([] { return &g_text; })),

		Function("FreeGlobal", std::function<void(void)>([](){ g_derived.reset(); })),
		Function("GetGlobal",  std::function<std::shared_ptr<D>(void)>([] () { return g_derived; })),
		Function("GetConstGlobal", std::function<const D*(void)>([]() { return g_derived.get(); })),
		Function("PrintBase",  std::function<void(std::shared_ptr<B>)>([] (std::shared_ptr<B> b) { std::cout << "Base: " << b->b << std::endl; })),

		Function("Factory",
			std::function<Object(const char*)>([&state](const char *type) {
				if (!std::strcmp("D", type)) return Object(state, D());
				if (!std::strcmp("B", type)) return Object(state, B());
				return Object(state, nullptr);
		}))

	];

	if (!g_cUpperDecorator.Create(state.GetState(), "UpperDecorator.lua") ||
		!g_cPlainDecorator.Create(state.GetState(), "PlainDecorator.lua"))
	{
		std::cout << "Cannot load decorators" << std::endl;
		return 1;
	}

	g_derived = std::shared_ptr<D>(new D);

	g_derived->b = 1337;
	g_derived->d = 666;

	state.RunFile("test2.lua");

	std::string line;

	while (!std::cin.eof())
	{

		std::getline(std::cin, line);

		if (!line.empty())
		{
			state.RunCode(line.c_str());
		}

	}

	state.Destroy();

	return 0;

}