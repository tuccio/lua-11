function PrintDecoratedText(dt)
	print(dt:Decorate())
end

local p = GetPlainDecorator()
local u = GetUpperDecorator()

local gt = GetGlobalText()

print(gt.text)

PrintDecoratedText(p)
PrintDecoratedText(u)

local g = GetGlobal()

print(g.b)
print(g.d)

g.b = 999
g.d = 23

print(g.b)
print(g.d)

PrintBase(g)

print('Const test')

local cg = GetConstGlobal()

print(cg.b)
print(cg.d)

PrintBase(cg)

local dInst = Factory('D')
local bInst = Factory('B')

print(dInst.b)
print(dInst.d)

print(bInst.b)
print(bInst.d)

local unknown = Factory('Unknown Type')
print(unknown)
